﻿using Moq;
using Xunit;
using System;
using BLL.DTO;
using AutoMapper;
using BLL.Services;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Infrastructure;
using BLL.EqualityComparers;
using System.Threading.Tasks;
using System.Collections.Generic;
using Test.BLL.TestData.Services;

namespace Test.BLL.Services
{
    public class ProductServiceTests
    {
        readonly ProductService _sut;
        readonly MapperConfiguration _mapperMock;
        readonly Mock<IEFUnitOfWork> _databaseMock = new Mock<IEFUnitOfWork>();

        public ProductServiceTests()
        {
            _mapperMock = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MapperProfileBLL());
            });
            _sut = new ProductService(_databaseMock.Object, _mapperMock.CreateMapper());
        }

        #region GetGeneralProductByIdAsync_Tests
        [Theory]
        [ClassData(typeof(GetGeneralProductByIdAsyncTestData))]
        public async Task GetGeneralProductByIdAsync_Should_Return_GProduct_When_GProduct_Exists(
            GeneralProductDTO expected, GeneralProduct db_init, int id)
        {
            // Arrange
            _databaseMock.Setup(x => x.GeneralProducts.FindByIdAsync(id))
                .ReturnsAsync(db_init);

            // Act
            var actual = await _sut.GetGeneralProductByIdAsync(id);

            // Assert
            Assert.Equal(expected, actual, comparer: new GeneralProductComparer());
        }

        [Fact]
        public async Task GetGeneralProductByIdAsync_Throws_Exception_If_Incorrect_Id()
        {
            // Arrange
            _databaseMock.Setup(x => x.GeneralProducts.FindByIdAsync(It.IsAny<int>()))
                .ReturnsAsync(() => null);

            // Act
            var actual = await Assert.ThrowsAsync<Exception>(() => _sut.GetGeneralProductByIdAsync(It.IsAny<int>()));

            // Assert
            Assert.Equal("None of requested items found.", actual.Message);

        }
        #endregion

        [Theory/*(Skip = "Does not work as expected")*/]
        [ClassData(typeof(GetGeneralProductsAsyncTestData))]
        public async Task GetGeneralProductsAsync_Should_Return_List_Of_GProducts_When_GProducts_Exist(
            IEnumerable<GeneralProductDTO> expected, IEnumerable<GeneralProduct> db_init, int id)
        {
            // Arrange
            _databaseMock.Setup(x => x.GeneralProducts.GetByConditionAsync(null))
                .ReturnsAsync(db_init);

            // Act
            var actual = await _sut.GetGeneralProductsAsync(id);

            // Assert
            Assert.Equal(expected, actual, comparer: new GeneralProductComparer());
        }
        
        [Theory(Skip = "Does not work as expected in some cases")]
        [InlineData(1, 5)]
        [InlineData(1, -5)]
        [InlineData(1, 0)]
        //[InlineData(1, 1)]
        public async Task GetGeneralProductsAsync_Should_Return_Empty_List_If_No_Matches_Found
            (int test_data_id, int incorrect_id)
        {
            // Arrange
            var test_db_data = new List<GeneralProduct>() 
            {
                new GeneralProduct
                {
                    Id = 1,
                    CategoryId = test_data_id
                }
            };

            var expected = new List<GeneralProductDTO>() { };

            _databaseMock.Setup(x => x.GeneralProducts.GetByConditionAsync(gp => gp.CategoryId == test_data_id))
                .ReturnsAsync(test_db_data);

            // Act
            var actual = await _sut.GetGeneralProductsAsync(incorrect_id);

            // Assert
            Assert.Equal(expected, actual);

        }
    }
}
