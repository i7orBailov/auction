﻿using BLL.DTO;
using DAL.Entities;
using System.Collections;
using System.Collections.Generic;

namespace Test.BLL.TestData.Services
{
    public class GetGeneralProductByIdAsyncTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new GeneralProductDTO
                {
                    Id = 1,
                    Image = "image",
                    Name = "product name",
                    CategoryId = 5
                },
                new GeneralProduct
                {
                    Id = 1,
                    Image = "image",
                    Name = "product name",
                    CategoryId = 5
                }, 1
            };
            yield return new object[]
            {
                new GeneralProductDTO
                {
                    Id = 9,
                    Image = "picture",
                    Name = "production",
                    CategoryId = 11
                },
                new GeneralProduct
                {
                    Id = 9,
                    Image = "picture",
                    Name = "production",
                    CategoryId = 11
                }, 9
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class GetGeneralProductsAsyncTestData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new List<GeneralProductDTO>
                {
                    new GeneralProductDTO
                    {
                        Id = 1,
                        Image = "image",
                        Name = "product name",
                        CategoryId = 1
                    },
                    new GeneralProductDTO
                    {
                        Id = 3,
                        Image = "image-2",
                        Name = "product name-2",
                        CategoryId = 1
                    }
                },
                new List<GeneralProduct>
                {
                    new GeneralProduct
                    {
                        Id = 1,
                        Image = "image",
                        Name = "product name",
                        CategoryId = 1
                    },
                    new GeneralProduct
                    {
                        Id = 3,
                        Image = "image-2",
                        Name = "product name-2",
                        CategoryId = 1
                    }
                }, 
                1
            };
            yield return new object[]
            {
                new List<GeneralProductDTO>
                {
                    new GeneralProductDTO { },
                    new GeneralProductDTO
                    {
                        Id = 5,
                        Image = "picture",
                        Name = "production",
                        CategoryId = 9
                    }
                },
                new List<GeneralProduct>
                {
                    new GeneralProduct { },
                    new GeneralProduct
                    {
                        Id = 5,
                        Image = "picture",
                        Name = "production",
                        CategoryId = 9
                    }
                }, 
                9
            };

        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
