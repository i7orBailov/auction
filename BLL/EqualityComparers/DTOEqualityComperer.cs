﻿using BLL.DTO;
using System.Collections.Generic;

namespace BLL.EqualityComparers
{
    public class BidComparer : IEqualityComparer<BidDTO>
    {
        public bool Equals(BidDTO x, BidDTO y)
        {
            if (x is null && y is null)
                return true;
            else if (x is null || y is null)
                return false;
            else if (x.Id.Equals(y.Id) &&
                     x.ClientId.Equals(y.ClientId) &&
                     x.InitialPrice.Equals(y.InitialPrice) &&
                     x.SuggestedPrice.Equals(y.SuggestedPrice) &&
                     x.ParticularProductId.Equals(y.ParticularProductId))
                return true;
            else
                return false;
        }

        public int GetHashCode(BidDTO obj) => base.GetHashCode();
    }

    public class CategoryComparer : IEqualityComparer<CategoryDTO>
    {
        public bool Equals(CategoryDTO x, CategoryDTO y)
        {
            if (x is null && y is null)
                return true;
            else if (x is null || y is null)
                return false;
            else if (x.Id.Equals(y.Id) &&
                     x.Title.Equals(y.Title) &&
                     x.Image.Equals(y.Image))
                return true;
            else
                return false;
        }

        public int GetHashCode(CategoryDTO obj) => base.GetHashCode();
    }

    public class ClientComparer : IEqualityComparer<ClientDTO>
    {
        public bool Equals(ClientDTO x, ClientDTO y)
        {
            if (x is null && y is null)
                return true;
            else if (x is null || y is null)
                return false;
            else if (x.Id.Equals(y.Id) &&
                     x.Role.Equals(y.Role) &&
                     x.Email.Equals(y.Email) &&
                     x.Address.Equals(y.Address) &&
                     x.LastName.Equals(y.LastName) && 
                     x.NickName.Equals(y.NickName) &&
                     x.Password.Equals(y.Password) &&
                     x.FirstName.Equals(y.FirstName))
                return true;
            else
                return false;
        }

        public int GetHashCode(ClientDTO obj) => base.GetHashCode();
    }

    public class ConditionComparer : IEqualityComparer<ConditionDTO>
    {
        public bool Equals(ConditionDTO x, ConditionDTO y)
        {
            if (x is null && y is null)
                return true;
            else if (x is null || y is null)
                return false;
            else if (x.Id.Equals(y.Id) &&
                     x.State.Equals(y.State))
                return true;
            else
                return false;
        }

        public int GetHashCode(ConditionDTO obj) => base.GetHashCode();
    }

    public class GeneralProductComparer : IEqualityComparer<GeneralProductDTO>
    {
        public bool Equals(GeneralProductDTO x, GeneralProductDTO y)
        {
            if (x is null && y is null)
                return true;
            else if (x is null || y is null)
                return false;
            else if (x.Id.Equals(y.Id) &&
                     x.Name.Equals(y.Name) &&
                     x.Image.Equals(y.Image) &&
                     x.CategoryId.Equals(y.CategoryId))
                return true;
            else
                return false;
        }

        public int GetHashCode(GeneralProductDTO obj) => base.GetHashCode();
    }

    public class OrderComparer : IEqualityComparer<OrderDTO>
    {
        public bool Equals(OrderDTO x, OrderDTO y)
        {
            if (x is null && y is null)
                return true;
            else if (x is null || y is null)
                return false;
            else if (x.Id.Equals(y.Id) &&
                     x.BidId.Equals(y.BidId) &&
                     x.ProcessingDate.Equals(y.ProcessingDate) &&
                     x.ShippingAddress.Equals(y.ShippingAddress))
                return true;
            else
                return false;
        }

        public int GetHashCode(OrderDTO obj) => base.GetHashCode();
    }

    public class ParticularProductComparer : IEqualityComparer<ParticularProductDTO>
    {
        public bool Equals(ParticularProductDTO x, ParticularProductDTO y)
        {
            if (x is null && y is null)
                return true;
            else if (x is null || y is null)
                return false;
            else if (x.Id.Equals(y.Id) &&
                     x.Name.Equals(y.Name) &&
                     x.SellerId.Equals(y.SellerId) &&
                     x.Description.Equals(y.Description) &&
                     x.CurrentPrice.Equals(y.CurrentPrice) &&
                     x.ExpirationDate.Equals(y.ExpirationDate) &&
                     x.GeneralProductId.Equals(y.GeneralProductId))
                return true;
            else
                return false;
        }

        public int GetHashCode(ParticularProductDTO obj) => base.GetHashCode();
    }

    public class PictureComparer : IEqualityComparer<PictureDTO>
    {
        public bool Equals(PictureDTO x, PictureDTO y)
        {
            if (x is null && y is null)
                return true;
            else if (x is null || y is null)
                return false;
            else if (x.Id.Equals(y.Id) &&
                     x.URL.Equals(y.URL))
                return true;
            else
                return false;
        }

        public int GetHashCode(PictureDTO obj) => base.GetHashCode();
    }
}
