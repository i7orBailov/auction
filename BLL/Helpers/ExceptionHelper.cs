﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace BLL.Helpers
{
    public class ExceptionHelper
    {
        static string DEFAULT_EXCEPTION_MESSAGE = "None of requested items found.";

        public static void ThrowIfNull<T>(T item, string exceptionMessage)
        {
            if (default(T) == null && item == null)
                throw new Exception(exceptionMessage);
        }

        public static void ThrowIfNull<T>(T item) => ThrowIfNull(item, DEFAULT_EXCEPTION_MESSAGE);

        public static void ThrowIfNull<T>(IEnumerable<T> collection, string exceptionMessage)
        {
            if (collection.Count() is 0)
                throw new Exception(exceptionMessage);
        }

        public static void ThrowIfNull<T>(IEnumerable<T> collection) =>
            ThrowIfNull(collection, DEFAULT_EXCEPTION_MESSAGE);
    }
}
