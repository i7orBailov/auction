﻿using System.Collections.Generic;

namespace BLL.DTO
{
    public class ClientDTO
    {
        public string Id { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string NickName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public virtual ICollection<OrderDTO> Orders { get; set; }
        public virtual ICollection<BidDTO> Bids { get; set; }
    }
}
