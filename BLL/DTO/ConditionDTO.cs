﻿using System.Collections.Generic;

namespace BLL.DTO
{
    public class ConditionDTO
    {
        public int Id { get; set; }
        public string State { get; set; }
        public virtual ICollection<OrderDTO> Orders { get; set; }
    }
}
