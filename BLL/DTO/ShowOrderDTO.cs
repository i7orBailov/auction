﻿using System;

namespace BLL.DTO
{
    public class ShowOrderDTO
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int ConditionId { get; set; }
        public string ProductName { get; set; }
        public bool IsSentToCustomer { get; set; }
        public string ProductCategory { get; set; }
        public string ShippingAddress { get; set; }
        public DateTime ProcessingDate { get; set; }
    }
}
