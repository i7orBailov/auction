﻿namespace BLL.DTO
{
    public class PictureDTO
    {
        public int Id { get; set; }
        public string URL { get; set; }
        public int ParticularProductId { get; set; }
    }
}
