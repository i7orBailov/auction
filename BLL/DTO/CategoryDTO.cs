﻿using System.Collections.Generic;

namespace BLL.DTO
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public ICollection<GeneralProductDTO> Products { get; set; }
    }
}
