﻿using System;

namespace BLL.DTO
{
    public class OrderDTO
    {
        public int Id { get; set; }
        public int BidId { get; set; }
        public virtual BidDTO Bid { get; set; }
        public string ShippingAddress { get; set; }
        public DateTime ProcessingDate { get; set; }
    }
}
