﻿using BLL.EqualityComparers;

namespace BLL.DTO
{
    public class BidDTO : BidComparer
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public decimal InitialPrice { get; set; }
        public decimal SuggestedPrice { get; set; }
        public int ParticularProductId { get; set; }
        public string BidLastCustomerId { get; set; }
    }
}
