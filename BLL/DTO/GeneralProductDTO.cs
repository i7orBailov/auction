﻿using System.Collections.Generic;

namespace BLL.DTO
{
    public class GeneralProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public int CategoryId { get; set; }
        public CategoryDTO Category { get; set; }
        public ICollection<ParticularProductDTO> ParticularProducts { get; set; }
    }
}
