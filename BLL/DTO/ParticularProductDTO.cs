﻿using System;
using System.Collections.Generic;

namespace BLL.DTO
{
    public class ParticularProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SellerId { get; set; }
        public string PictureURL { get; set; }
        public string Description { get; set; }
        public decimal CurrentPrice { get; set; }
        public int GeneralProductId { get; set; }
        public DateTime ExpirationDate { get; set; }
        public virtual ICollection<BidDTO> Bids { get; set; }
        public virtual GeneralProductDTO GeneralProduct { get; set; }
        public virtual ICollection<PictureDTO> Pictures { get; set; }
    }
}
