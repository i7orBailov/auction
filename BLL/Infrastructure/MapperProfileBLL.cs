﻿using BLL.DTO;
using AutoMapper;
using System.Linq;
using DAL.Entities;

namespace BLL.Infrastructure
{
    public class MapperProfileBLL : Profile
    {
        public MapperProfileBLL()
        {
            CreateMap<GeneralProduct, GeneralProductDTO>()
                .ForMember(gp => gp.ParticularProducts, x => x.Ignore())
                .ReverseMap();
            CreateMap<Category, CategoryDTO>().ForMember(p => p.Products, x => x.Ignore());
            CreateMap<ParticularProduct, ParticularProductDTO>()
                .ForMember(pp => pp.Bids, x => x.Ignore())
                .ReverseMap();
            CreateMap<Picture, PictureDTO>().ReverseMap();
            CreateMap<Client, ClientDTO>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.ApplicationUser.Email))
                .ForMember(dest => dest.NickName, opt => opt.MapFrom(src => src.ApplicationUser.UserName))
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.ApplicationUser.Roles
                                                                 .FirstOrDefault(r => r.UserId == src.Id).RoleId))
                .ForMember(dest => dest.Bids, opt => opt.Ignore())
                .ForMember(dest => dest.Orders, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<Bid, BidDTO>()
                .ForMember(dest => dest.InitialPrice, opt => opt.MapFrom(src => src.ParticularProduct.CurrentPrice));
            CreateMap<BidDTO, Bid>()
                .ForMember(dest => dest.ParticularProduct, opt => opt.Ignore());
            CreateMap<ApplicationUser, ClientDTO>()
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.ClientProfile.Address))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.ClientProfile.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.ClientProfile.LastName))
                .ReverseMap();
            CreateMap<ExceptionDetail, ExceptionDetailDTO>()
                .ReverseMap();
            CreateMap<Order, ShowOrderDTO>()
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Bid.SuggestedPrice))
                .ForMember(dest => dest.ShippingAddress, opt => opt.MapFrom(src => src.Client.Address))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Bid.ParticularProduct.Name))
                .ForMember(dest => dest.ProductCategory, opt => opt.MapFrom(src => src.Bid.ParticularProduct.GeneralProduct.Category.Title));
        }
    }
}
