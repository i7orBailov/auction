﻿using DAL.EF;
using BLL.Services;
using BLL.Interfaces;
using DAL.Interfaces;
using Ninject.Modules;
using DAL.Repositories;
using BLL.Services.AdminServices;
using BLL.Interfaces.AdminInterfaces;

namespace BLL.Infrastructure
{
    public class ServiceModuleBLL : NinjectModule
    {
        public ServiceModuleBLL() { }

        public override void Load()
        {
            Bind<IBidService>().To<BidService>();
            Bind<IOrderService>().To<OrderService>();
            Bind<IManageService>().To<ManageService>();
            Bind<IProductService>().To<ProductService>();
            Bind<IIdentityUnitOfWork>().To<EFUnitOfWork>();
            Bind<IExceptionUnitOfWork>().To<EFUnitOfWork>();
            Bind<IAdminUserService>().To<AdminUserService>();
            Bind<IAdminProductService>().To<AdminProductService>();
            Bind<IEFUnitOfWork>().To<EFUnitOfWork>().WithConstructorArgument(new AuctionContext());
        }
    }
}
