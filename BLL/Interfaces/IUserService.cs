﻿using System;
using BLL.DTO;
using BLL.Infrastructure;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        string GetRoleOfUser();
        Task<OperationDetails> CreateAsync(ClientDTO userDto);
        Task<ClaimsIdentity> AuthenticateAsync(ClientDTO userDto);
    }
}
