﻿using System;
using BLL.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    public interface IAdminProductService : IDisposable
    {
        Task DeleteProductAsync(int? productId);
        Task AcceptRequestForProductSaleAsync(int? id);
        Task DeclineRequestForProductSaleAsync(int? id);
        Task<ParticularProductDTO> GetProductWaitingForConfirmationAsync(int? id);
        Task<IEnumerable<ParticularProductDTO>> GetProductsWaitingForConfirmationAsync();
    }
}
