﻿using BLL.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BLL.Interfaces.AdminInterfaces
{
    public interface IAdminUserService
    {
        Task<IEnumerable<ClientDTO>> GetAllUsersAsync();
        Task<ClientDTO> PromoteAndGetUserAsync(string id);
    }
}
