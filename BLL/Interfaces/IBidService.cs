﻿using System;
using BLL.DTO;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IBidService : IDisposable
    {
        Task MakeBidAsync(BidDTO bidDto);
        Task<string> GetProductOwnerId(int? productId);
        Task<string> GetLastCustomerIdAsync(int? productId);
        Task<decimal> GetProductInitialPriceAsync(int? productId);
    }
}
