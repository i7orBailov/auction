﻿namespace BLL.Interfaces
{
    public interface IUserBackgroundService
    {
        bool HasPersonalInfo(string userId);
    }
}
