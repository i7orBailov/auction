﻿using System;
using BLL.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    public interface IOrderService : IDisposable
    {
        Task AutoGenerateOrderAsync();
        Task RemoveOrder(int? id, string customerId);
        Task AcceptOrder(int? id, string customerId);
        Task<IEnumerable<ShowOrderDTO>> GetAllOrdersAsync(string userId);
    }
}
