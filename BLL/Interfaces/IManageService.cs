﻿using System;
using BLL.DTO;
using DAL.Entities;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using BLL.Infrastructure;

namespace BLL.Interfaces
{
    public interface IManageService : IDisposable
    {
        Task<IdentityResult> ChangePasswordAsync(string userId, string oldP, string newP);
        Task<ApplicationUser> FindByIdAsync(string userId);
        Task<IdentityResult> SetPersonalInfoAsync(ClientDTO userDto);
        Task<ClientDTO> GetPersonalInfoByIdAsync(string userId);
    }
}
