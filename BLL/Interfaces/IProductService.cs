﻿using System;
using BLL.DTO;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    public interface IProductService : IDisposable
    {
        Task<IEnumerable<CategoryDTO>> GetCategoriesAsync();
        Task SellNewProductAsync(ParticularProductDTO productDto);
        Task<ParticularProductDTO> GetParticularProductByIdAsync(int? id);
        Task<IEnumerable<GeneralProductDTO>> GetGeneralProductsAsync(int? id);
        Task<GeneralProductDTO> GetGeneralProductByIdAsync(int productToGetId);
        Task<IEnumerable<ParticularProductDTO>> GetParticularProductListAsync(int? id);
        Task<IEnumerable<ParticularProductDTO>> GetParticularProductListAsync(string name);
    }
}
