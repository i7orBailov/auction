﻿using BLL.DTO;
using AutoMapper;
using System.Linq;
using BLL.Helpers;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class BidService : IBidService
    {
        readonly IMapper _mapper;
        readonly IEFUnitOfWork _database;

        public BidService(IEFUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;
        }

        public async Task MakeBidAsync(BidDTO bidDto)
        {
            ExceptionHelper.ThrowIfNull(bidDto);
            var bid = _mapper.Map<Bid>(bidDto);
            await _database.Bids.CreateAsync(bid);
            await SetNewCurrentPrice(bidDto.ParticularProductId, bidDto.SuggestedPrice);
        }

        async Task SetNewCurrentPrice(int productId, decimal newPrice)
        {
            var product = await GetProductAsync(productId);
            product.CurrentPrice = newPrice;
            await _database.ParticularProducts.ModifyAsync(product);
        }

        public async Task<decimal> GetProductInitialPriceAsync(int? productId)
        {
            var productPrice = (await GetProductAsync(productId)).CurrentPrice;
            return productPrice;
        }

        public async Task<string> GetLastCustomerIdAsync(int? productId)
        {
            var lastCustomerId = (await GetProductAsync(productId))
                                  .Bids.OrderBy(b => b.SuggestedPrice).LastOrDefault()?.ClientId;
            return lastCustomerId;
        }

        public async Task<string> GetProductOwnerId(int? productId)
        {
            var ownerId = (await GetProductAsync(productId)).SellerId;
            return ownerId;
        }

        async Task<ParticularProduct> GetProductAsync(int? productId)
        {
            var product = await _database.ParticularProducts.FindByIdAsync(productId);
            ExceptionHelper.ThrowIfNull(product);
            return product;
        }

        public void Dispose() => _database.Dispose();
    }
}
