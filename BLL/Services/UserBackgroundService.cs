﻿using DAL.EF;
using System.Linq;
using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Repositories;

namespace BLL.Services
{
    public class UserBackgroundService : IUserBackgroundService
    {
        readonly IEFUnitOfWork _database;

        public UserBackgroundService()
        {
            _database = new EFUnitOfWork(new AuctionContext());
        }

        public bool HasPersonalInfo(string userId)
        {
            var user = _database.Clients.GetByCondition(x => x.Id.Equals(userId)).FirstOrDefault();
            if (user is null)
                return false;

            return !(user.Address is null &&
                   user.FirstName is null &&
                   user.LastName is null);
        }
    }
}
