﻿using BLL.DTO;
using AutoMapper;
using BLL.Helpers;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace BLL.Services
{
    public class ManageService : IManageService
    {
        readonly IMapper _mapper;
        readonly IIdentityUnitOfWork _database;

        public ManageService(IIdentityUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;
        }

        public async Task<IdentityResult> ChangePasswordAsync(string userId, string oldP, string newP) =>
            await _database.UserManager.ChangePasswordAsync(userId, oldP, newP);

        public async Task<ApplicationUser> FindByIdAsync(string userId) =>
            await _database.UserManager.FindByIdAsync(userId);

        public async Task<IdentityResult> SetPersonalInfoAsync(ClientDTO userDto)
        {
            ExceptionHelper.ThrowIfNull(userDto);
            var applicationUser = await FindByIdAsync(userDto.Id);
            AlterAccountInfo(ref applicationUser, userDto.Email, userDto.FirstName,
                             userDto.LastName, userDto.Address);
            return await _database.UserManager.UpdateAsync(applicationUser);
        }

        public async Task<ClientDTO> GetPersonalInfoByIdAsync(string userId)
        {
            var user = await FindByIdAsync(userId);
            ExceptionHelper.ThrowIfNull(user);
            return _mapper.Map<ClientDTO>(user);
        }

        void AlterAccountInfo(ref ApplicationUser user, string mail, string firstName,
                              string lastName, string address)
        {
            user.Email = mail;
            user.ClientProfile.Address = address;
            user.ClientProfile.FirstName = firstName;
            user.ClientProfile.LastName = lastName;
        }

        public void Dispose() => _database.Dispose();
    }
}
