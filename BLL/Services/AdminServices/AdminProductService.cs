﻿using BLL.DTO;
using AutoMapper;
using BLL.Helpers;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BLL.Services
{
    public class AdminProductService : IAdminProductService
    {
        readonly IMapper _mapper;
        readonly IEFUnitOfWork _efDatabase;
        readonly IIdentityUnitOfWork _identityDatabase;
        
        public AdminProductService(IIdentityUnitOfWork iUow, IMapper mapper, IEFUnitOfWork efUow)
        {
            _mapper = mapper;
            _efDatabase = efUow;
            _identityDatabase = iUow;
        }

        public async Task<IEnumerable<ParticularProductDTO>> GetProductsWaitingForConfirmationAsync()
        {
            var products = await _efDatabase.ParticularProducts.GetByConditionAsync(pp => pp.StatusId == 2);
            var productsDto = _mapper.Map<IEnumerable<ParticularProduct>, IEnumerable<ParticularProductDTO>>(products);
            return productsDto;
        }

        public async Task<ParticularProductDTO> GetProductWaitingForConfirmationAsync(int? productsId)
        {
            var product = (await _efDatabase.ParticularProducts.GetByConditionAsync(pp => pp.Id == productsId &&
                                                                                          pp.StatusId == 2))
                          .FirstOrDefault();
            ExceptionHelper.ThrowIfNull(product);
            var productDto = _mapper.Map<ParticularProductDTO>(product);
            return productDto;
        }

        public async Task DeclineRequestForProductSaleAsync(int? productId) =>
            await _efDatabase.ParticularProducts.RemoveAsync(await GetProductByIdAsync(productId));

        public async Task AcceptRequestForProductSaleAsync(int? productId) =>
            await SetStatus(await GetProductByIdAsync(productId), newStatusId: 1);

        async Task SetStatus(ParticularProduct product, int newStatusId)
        {
            product.StatusId = newStatusId;
            await _efDatabase.ParticularProducts.ModifyAsync(product);
        }

        async Task<ParticularProduct> GetProductByIdAsync(int? productId)
        {
            var product = await _efDatabase.ParticularProducts.FindByIdAsync(productId);
            ExceptionHelper.ThrowIfNull(product);
            return product;
        }

        public async Task DeleteProductAsync(int? productId) =>
            await _efDatabase.ParticularProducts.RemoveAsync(await GetProductByIdAsync(productId));

        public void Dispose()
        {
            _efDatabase.Dispose();
            _identityDatabase.Dispose();
        }
    }
}
