﻿using BLL.DTO;
using AutoMapper;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Collections.Generic;
using BLL.Interfaces.AdminInterfaces;
using BLL.Helpers;

namespace BLL.Services.AdminServices
{
    public class AdminUserService : IAdminUserService
    {
        readonly IMapper _mapper;
        readonly IEFUnitOfWork _database;
        readonly IIdentityUnitOfWork _identityDatabase;

        public AdminUserService(IEFUnitOfWork efUow, IMapper mapper, IIdentityUnitOfWork identityDatabase)
        {
            _mapper = mapper;
            _database = efUow;
            _identityDatabase = identityDatabase;
        }

        public async Task<IEnumerable<ClientDTO>> GetAllUsersAsync()
        {
            var users = await _database.Clients.GetAllAsync();
            var usersDto = _mapper.Map<IEnumerable<Client>, IEnumerable<ClientDTO>>(users);
            await AssignUserRoleAsync(usersDto);
            return usersDto;
        }

        async Task AssignUserRoleAsync(IEnumerable<ClientDTO> users)
        {
            var roleTitles = await _identityDatabase.RoleManager.Roles.ToListAsync();
            foreach (var user in users)
            {
                var userRole = roleTitles.FirstOrDefault(ur => ur.Id == user.Role).Name;
                user.Role = userRole;
            }
        }

        public async Task<ClientDTO> PromoteAndGetUserAsync(string id)
        {
            var user = (await _database.Clients.GetByConditionAsync(x => x.Id.Equals(id))).FirstOrDefault();
            ExceptionHelper.ThrowIfNull(user);
            await _identityDatabase.UserManager.AddToRoleAsync(id, "Admin");
            await _identityDatabase.SaveAsync();
            var userDto = _mapper.Map<Client, ClientDTO>(user);
            return userDto;
        }
    }
}
