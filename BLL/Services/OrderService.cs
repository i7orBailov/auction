﻿using System;
using DAL.EF;
using BLL.DTO;
using AutoMapper;
using BLL.Helpers;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using DAL.Repositories;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BLL.Services
{
    public class OrderService : IOrderService
    {
        readonly IMapper _mapper;
        readonly IEFUnitOfWork _database;

        public OrderService(IEFUnitOfWork db, IMapper mapper)
        {
            _database = db;
            _mapper = mapper;
        }

        public OrderService()
        {
            _database = new EFUnitOfWork(new AuctionContext());
        }

        public async Task<IEnumerable<ShowOrderDTO>> GetAllOrdersAsync(string userId)
        {
            var orders = await _database.Orders.GetByConditionAsync(o => o.ClientId.Equals(userId) &&
                                                                         o.ConditionId != 3);
            var ordersDto = GetOrdersDTO(orders);
            return ordersDto;
        }

        IEnumerable<ShowOrderDTO> GetOrdersDTO(IEnumerable<Order> orders)
        {
            var ordersDto = _mapper.Map<IEnumerable<Order>, IEnumerable<ShowOrderDTO>>(orders);
            foreach (var order in ordersDto)
            {
                if (order.ConditionId == 2 || order.ConditionId == 3)
                    order.IsSentToCustomer = true;
            }
            return ordersDto;
        }

        public async Task RemoveOrder(int? id, string customerId)
        {
            var order = await GetOrderAsync(conditionId: 1, id, customerId);
            ExceptionHelper.ThrowIfNull(order, "It is forbidden to remove order you don't own!");
            await _database.Orders.RemoveAsync(order);
        }

        public async Task AcceptOrder(int? id, string customerId)
        {
            var order = await GetOrderAsync(conditionId: 1, id, customerId);
            ExceptionHelper.ThrowIfNull(order, "It is forbidden to accept order you don't own!");
            await AlterOrderAsync(order);
        }
        
        async Task AlterOrderAsync(Order order)
        {
            order.ConditionId = 2;
            await _database.Orders.ModifyAsync(order);
        }

        async Task<Order> GetOrderAsync(int conditionId, int? id, string customerId)
        {
            var order = (await _database.Orders.GetByConditionAsync(o => o.Id == id &&
                               o.ClientId.Equals(customerId) &&
                               o.ConditionId == conditionId)).FirstOrDefault();
            return order;
        }

        public async Task AutoGenerateOrderAsync()
        {
            while (true)
            {
                try
                {
                    if (_database.Exists)
                    {
                        foreach (var product in await GetExpiredProductsAsync())
                        {
                            if (HasAnyBid(product))
                                await CreateNewOrderAsync(product);
                            else
                                await DeleteAsync(product);
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }
        }

        async Task<IEnumerable<ParticularProduct>> GetExpiredProductsAsync() 
        {
            var products = await _database.ParticularProducts.GetByConditionAsync(p => DateTime.Now >= p.ExpirationDate &&
                                                                           p.StatusId == 1);
            return products;
        }

        bool HasAnyBid(ParticularProduct product)
        {
            bool result = product.Bids.Any();
            return result;
        }

        async Task DeleteAsync(ParticularProduct product) =>
            await _database.ParticularProducts.RemoveAsync(product);

        async Task CreateNewOrderAsync(ParticularProduct product)
        {
            var highestBid = FindBidWithHighestSuggestedPrice(product);
            await SaveOrderAsync(highestBid);
            await AlterProductStatusAsync(product, newStatusId: 3);
        }

        Bid FindBidWithHighestSuggestedPrice(ParticularProduct product)
        {
            var lastBid = product.Bids.OrderBy(b => b.SuggestedPrice).FirstOrDefault();
            return lastBid;
        }

        async Task SaveOrderAsync(Bid bid)
        {
            Order order = new Order
            {
                BidId = bid.Id,
                ClientId = bid.ClientId
            };
            await _database.Orders.CreateAsync(order);
        }

        async Task AlterProductStatusAsync(ParticularProduct product, int newStatusId)
        {
            product.StatusId = newStatusId;
            await _database.ParticularProducts.ModifyAsync(product);
        }

        public void Dispose() => _database.Dispose();
    }
}
