﻿using DAL.EF;
using BLL.DTO;
using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ExceptionService
    {
        readonly IMapper _mapper;
        readonly IExceptionUnitOfWork _database;
        public ExceptionService()
        {
            _database = new EFUnitOfWork(new AuctionContext());
            _mapper = new Mapper(new MapperConfiguration(cfg => 
                                 cfg.CreateMap<ExceptionDetail, ExceptionDetailDTO>().ReverseMap()));
        }

        public void SaveException(ExceptionDetailDTO exceptionDto)
        {
            var exception = _mapper.Map<ExceptionDetail>(exceptionDto);
            _database.Exceptions.Create(exception);
        }
    }
}
