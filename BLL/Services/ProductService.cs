﻿using BLL.DTO;
using AutoMapper;
using BLL.Helpers;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace BLL.Services
{
    public class ProductService : IProductService
    {
        readonly IMapper _mapper;
        readonly IEFUnitOfWork _database;
        const string SELL_PRODUCT_ERROR = "Seems You are trying to add product to category which does not exist.";

        public ProductService(IEFUnitOfWork uow, IMapper mapper)
        {
            _database = uow;
            _mapper = mapper;
        }
        #region Category
        public async Task<IEnumerable<CategoryDTO>> GetCategoriesAsync()
        {
            var categories = await _database.Categories.GetAllAsync();
            var categoriesDto = _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDTO>>(categories);
            return categoriesDto;
        }
        #endregion

        #region GeneralProduct
        public async Task<IEnumerable<GeneralProductDTO>> GetGeneralProductsAsync(int? id)
        {
            var products = await _database.GeneralProducts.GetByConditionAsync(x => x.CategoryId == id);
            var productsDto = _mapper.Map<IEnumerable<GeneralProduct>, IEnumerable<GeneralProductDTO>>(products);
            return productsDto;
        }

        public async Task<GeneralProductDTO> GetGeneralProductByIdAsync(int productToGetId)
        {
            var product = await _database.GeneralProducts.FindByIdAsync(productToGetId);
            ExceptionHelper.ThrowIfNull(product);
            var productDto = _mapper.Map<GeneralProductDTO>(product);
            return productDto;
        }
        #endregion

        #region ParticularProduct
        public async Task<IEnumerable<ParticularProductDTO>> GetParticularProductListAsync(int? id)
        {
            var products = await _database.ParticularProducts.GetByConditionAsync(pp => pp.GeneralProductId == id && 
                                                                                       pp.StatusId == 1);
            var productsDto = _mapper.Map<IEnumerable<ParticularProduct>, IEnumerable<ParticularProductDTO>>(products);
            return productsDto;
        }

        public async Task<IEnumerable<ParticularProductDTO>> GetParticularProductListAsync(string name)
        {
            var products = await _database.ParticularProducts.GetByConditionAsync(pp => pp.GeneralProduct.Name
                                                             .ToLower().Contains(name.ToLower()) && pp.StatusId == 1);
            var productsDto = _mapper.Map<IEnumerable<ParticularProduct>, IEnumerable<ParticularProductDTO>>(products);
            return productsDto;
        }

        public async Task<ParticularProductDTO> GetParticularProductByIdAsync(int? id)
        {
            var product = (await _database.ParticularProducts.GetByConditionAsync(pp => pp.Id == id &&
                                                                                        pp.StatusId == 1))
                          .FirstOrDefault();
            ExceptionHelper.ThrowIfNull(product);
            var productDto = _mapper.Map<ParticularProduct, ParticularProductDTO>(product);
            return productDto;
        }
        #endregion

        #region SellNewProduct
        public async Task SellNewProductAsync(ParticularProductDTO productDto)
        {
            var product = _mapper.Map<ParticularProduct>(productDto);
            var generalProducts = await _database.GeneralProducts.FindByIdAsync(product.GeneralProductId);
            ExceptionHelper.ThrowIfNull(generalProducts, SELL_PRODUCT_ERROR);
            await _database.ParticularProducts.CreateAsync(product);
            await SetImage(product, productDto.PictureURL);
        }

        async Task SetImage(ParticularProduct product, string picture)
        {
            var pic = new Picture { ParticularProductId = product.Id };
            if (picture != null)
                pic.URL = picture;
            await _database.Pictures.CreateAsync(pic);
        }
        #endregion

        public void Dispose() => _database.Dispose();
    }
}
