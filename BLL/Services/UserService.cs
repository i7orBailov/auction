﻿using BLL.DTO;
using System.Linq;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using BLL.Infrastructure;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        const string USER_ROLE = "JustUser";
        readonly IIdentityUnitOfWork _database;

        public UserService(IIdentityUnitOfWork uow)
        {
            _database = uow;
        }

        public async Task<OperationDetails> CreateAsync(ClientDTO userDto)
        {
            ApplicationUser user = await _database.UserManager.FindByNameAsync(userDto.NickName);
            if (user is null)
            {
                user = new ApplicationUser
                {
                    Email = userDto.Email,
                    UserName = userDto.NickName
                };
                var result = await _database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), string.Empty);
                await _database.UserManager.AddToRoleAsync(user.Id, userDto.Role);
                Client clientProfile = new Client
                {
                    Address = userDto.Address,
                    FirstName = userDto.FirstName,
                    LastName = userDto.LastName,
                    Id = user.Id
                };
                _database.ClientManager.Create(clientProfile);
                await _database.SaveAsync();
                return new OperationDetails(succedeed: true, "Registered successfully.", string.Empty);
            }
            else
                return new OperationDetails(succedeed: false, "User already exists.", nameof(userDto.NickName));
        }

        public async Task<ClaimsIdentity> AuthenticateAsync(ClientDTO userDto)
        {
            ClaimsIdentity claim = null;
            ApplicationUser user = await _database.UserManager.FindAsync(userDto.NickName, userDto.Password);
            if (user != null)
                claim = await _database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        public string GetRoleOfUser() =>
            _database.RoleManager.Roles.FirstOrDefault(ur => ur.Name.Equals(USER_ROLE)).Name;

        public void Dispose() => _database.Dispose();
    }
}
