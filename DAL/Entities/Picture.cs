﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Picture
    {
        public int Id { get; set; }

        //[Required]
        public string URL { get; set; } = "~/Assets/Img/default-image-1.png";

        //[Required]
        public int ParticularProductId { get; set; }

        public virtual ParticularProduct ParticularProduct { get; set; }
    }
}
