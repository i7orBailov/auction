﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class ProductStatus
    {
        public int Id { get; set; }
        public string Title { get; set; } = "Waiting confirmation";
        public virtual ICollection<ParticularProduct> ParticularProducts { get; set; }
    }
}
