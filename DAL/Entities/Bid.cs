﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Bid
    {
        public int Id { get; set; }

        //[Required]
        public decimal SuggestedPrice { get; set; }

        [Required]
        public string ClientId { get; set; }

        public virtual Client Client { get; set; }

        //[Required]
        public int ParticularProductId { get; set; }

        public virtual ParticularProduct ParticularProduct { get; set; } 
    }
}
