﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class GeneralProduct
    {
        public int Id { get; set; }

        //[Required]
        public string Name { get; set; }
        public string Image { get; set; }

        //[Required]
        public int CategoryId { get; set; }

        public virtual Category Category { get; set; }

        public virtual ICollection<ParticularProduct> ParticularProducts { get; set; }
    }
}
