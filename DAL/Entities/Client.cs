﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    public class Client
    {
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }

        //[Required]
        public string FirstName { get; set; }

       // [Required]
        public string LastName { get; set; }

        public string Address { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<Bid> Bids { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
