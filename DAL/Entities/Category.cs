﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Category
    {
        public int Id { get; set; }

        //[Required]
        public string Title { get; set; }
        public string Image { get; set; }

        public virtual ICollection<GeneralProduct> Products { get; set; }
    }
}
