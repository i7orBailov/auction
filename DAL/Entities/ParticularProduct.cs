﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class ParticularProduct
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal CurrentPrice { get; set; }
        public DateTime ExpirationDate { get; set; } = DateTime.Now.AddDays(1);
        public int GeneralProductId { get; set; }
        public virtual GeneralProduct GeneralProduct { get; set; }
        public string SellerId { get; set; }
        public string Name { get; set; }
        public virtual ApplicationUser Seller { get; set; }
        public int? StatusId { get; set; } = 2;
        public virtual ProductStatus Status { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
        public virtual ICollection<Bid> Bids { get; set; }
    }
}
