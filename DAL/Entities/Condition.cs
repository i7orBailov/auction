﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Condition
    {
        public int Id { get; set; }

       //[Required]
        public string State { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
