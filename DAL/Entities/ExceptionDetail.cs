﻿using System;

namespace DAL.Entities
{
    public class ExceptionDetail
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string ActionName { get; set; }
        public string StackTrace { get; set; }
        public string ControllerName { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
