﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public int BidId { get; set; }
        public virtual Bid Bid { get; set; }
        public string ClientId { get; set; }
        public int ConditionId { get; set; } = 1;
        public virtual Client Client { get; set; }
        public virtual Condition Condition { get; set; }
        public DateTime ProcessingDate { get; set; } = DateTime.Now;
    }
}
