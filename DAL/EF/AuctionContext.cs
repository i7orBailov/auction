﻿using DAL.Entities;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.EF
{
    public class AuctionContext : IdentityDbContext<ApplicationUser>
    {
        public AuctionContext() : base("AuctionContext")
        {
            Database.SetInitializer(new AuctionDbInitializer());
        }

        public DbSet<Bid> Bids { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Condition> Conditions { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<ParticularProduct> ParticularProducts { get; set; }
        public DbSet<GeneralProduct> GeneralProducts { get; set; }
        public DbSet<ProductStatus> ProductStatus { get; set; }
        public DbSet<ExceptionDetail> ExceptionDetails { get; set; }
        public bool Exists { get { return Database.Exists(); } }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bid>().HasKey(b => b.Id);
            modelBuilder.Entity<Category>().HasKey(c => c.Id);
            modelBuilder.Entity<Client>().HasKey(c => c.Id);
            modelBuilder.Entity<Condition>().HasKey(c => c.Id);
            modelBuilder.Entity<GeneralProduct>().HasKey(gp => gp.Id);
            modelBuilder.Entity<Order>().HasKey(o => o.Id);
            modelBuilder.Entity<ParticularProduct>().HasKey(pp => pp.Id)
                                                    .HasMany(x => x.Bids)
                                                    .WithRequired(b => b.ParticularProduct);
            modelBuilder.Entity<Picture>().HasKey(p => p.Id);
            modelBuilder.Entity<ProductStatus>().HasKey(ps => ps.Id);
            modelBuilder.Entity<Client>().Property(c => c.FirstName).HasMaxLength(30);
            modelBuilder.Entity<Client>().Property(c => c.LastName).HasMaxLength(30);

            base.OnModelCreating(modelBuilder);
        }
    }
}
