﻿using DAL.Entities;
using DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;

namespace DAL.EF
{
    public class AuctionDbInitializer : DropCreateDatabaseAlways<AuctionContext>
    {
        protected override void Seed(AuctionContext context)
        {
            AddApplicationRoles(database: context);
            AddTestApplicationUsers(context);

            context.Conditions.AddRange(InitHelper.GetConditions());
            context.Categories.AddRange(InitHelper.GetCategories());
            context.GeneralProducts.AddRange(InitHelper.GetGeneralProducts());
            context.ParticularProducts.AddRange(InitHelper.GetParticularProducts());
            InitHelper.SetNameForEachPProduct();
            context.ProductStatus.AddRange(InitHelper.GetProductStatuses());
            context.Pictures.AddRange(InitHelper.GetPictures());
            
            //context.Bids.AddRange(InitHelper.GetBids());
            //context.Orders.AddRange(InitHelper.GetOrders());

            var clients = context.Clients;
            var bids = GetTestBids(clients.ToArray(), InitHelper.GetParticularProducts().ToArray());
            var orders = GetTestOrders(bids, InitHelper.GetConditions().ToArray(), clients.ToArray());

            context.Bids.AddRange(bids);
            context.Orders.AddRange(orders);
            context.SaveChanges();
        }

        void AddTestApplicationUsers(AuctionContext db)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));

            var admin = new ApplicationUser 
            { 
                Email = "admin@mail.co", 
                UserName = "SampleAdmin", 
                ClientProfile = new Client 
                { 
                    Address = "Earth",
                    FirstName = "Adam",
                    LastName = "Godson"
                }
            };

            var user = new ApplicationUser 
            {
                Email = "user@mail.co", 
                UserName = "SampleUser", 
                ClientProfile = new Client
                {
                    Address = "Earth", 
                    FirstName = "Eva",
                    LastName = "Goddauhter"
                } 
            };
            string password = "Pass1@";

            userManager.Create(user, password);
            userManager.Create(admin, password);
            
            userManager.AddToRole(admin.Id, "Admin"); 
            userManager.AddToRole(user.Id, "JustUser");
        }

        Bid[] GetTestBids(Client[] clients, ParticularProduct[] particularProducts)
        {
            Bid[] bids =
            {
                new Bid
                {
                    Client = clients[0],
                    ParticularProduct = particularProducts[0],
                    SuggestedPrice = 380
                },
                new Bid
                {
                    Client = clients[1],
                    ParticularProduct = particularProducts[0],
                    SuggestedPrice = 400
                },
                new Bid
                {
                    Client = clients[0],
                    ParticularProduct = particularProducts[1],
                    SuggestedPrice = 370
                },
                new Bid
                {
                    Client = clients[1],
                    ParticularProduct = particularProducts[1],
                    SuggestedPrice = 390
                },
                new Bid
                {
                    Client = clients[1],
                    ParticularProduct = particularProducts[2],
                    SuggestedPrice = 500
                },
                new Bid
                {
                    Client = clients[0],
                    ParticularProduct = particularProducts[2],
                    SuggestedPrice = 700
                },
                new Bid
                {
                    Client = clients[1],
                    ParticularProduct = particularProducts[7],
                    SuggestedPrice = 250
                }
            };
            return bids;
        }

        Order[] GetTestOrders(Bid[] bids, Condition[] conditions, Client[] clients)
        {
            Order[] orders =
            {
                new Order
                {
                   Bid = bids[1],
                   Client = clients[0],
                   ProcessingDate = GetRandomDate(),
                   Condition = conditions[0]
                },
                new Order
                {
                    Bid = bids[3],
                    Client = clients[0],
                    ProcessingDate = GetRandomDate(),
                    Condition = conditions[0]
                },
                new Order
                {
                    Bid = bids[5],
                    Client = clients[1],
                    ProcessingDate = GetRandomDate(),
                    Condition = conditions[1]
                },
                new Order
                {
                    Bid = bids[6],
                    Client = clients[1],
                    ProcessingDate = GetRandomDate(),
                    Condition = conditions[0]
                }
            };
            return orders;
        }

        static DateTime GetRandomDate()
        {
            var rnd = new Random();
            DateTime start = new DateTime(2020, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(rnd.Next(maxValue: range));
        }

        void AddApplicationRoles(AuctionContext database)
        {
            var roles = IdentityInitHelper.GetApplicationRoles();
            foreach (var role in roles)
            {
                database.Roles.Add(role);
            }
        }
    }
}
