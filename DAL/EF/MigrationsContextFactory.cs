﻿
using System.Data.Entity.Infrastructure;

namespace DAL.EF
{
    public class MigrationsContextFactory : IDbContextFactory<AuctionContext>
    {
        public AuctionContext Create()
        {
            return new AuctionContext();
        }
    }
}
