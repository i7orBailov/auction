﻿using System.Linq;
using DAL.Entities;
using DAL.Identity;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.EF
{
    public class IdentityInitHelper
    {
        static readonly string DEFAULT_TEST_PASSWORD = "Pass1@";
        readonly ApplicationUserManager userManager;
        readonly AuctionContext db;
        public IdentityInitHelper(AuctionContext db)
        {
            this.db = db;
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
        }

        static readonly ApplicationRole[] applicationRoles =
        {
            new ApplicationRole { Name = "Admin" },
            new ApplicationRole { Name = "JustUser" }
        };

        static readonly ApplicationUser[] applicationUsers =
        {
            new ApplicationUser
            {
                UserName = "SampleUser1",
                Email = "user1@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Lola",
                    LastName = "Aboba",
                },
            },
            new ApplicationUser
            {
                UserName = "SampleUser2",
                Email = "user2@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Ihor",
                    LastName = "Petridze"
                }
            },
            new ApplicationUser
            {
                UserName = "SampleUser3",
                Email = "user3@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Ivan",
                    LastName = "Koshebin",
                },
            },
            new ApplicationUser
            {
                UserName = "SampleUser4",
                Email = "user4@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Izolda",
                    LastName = "Petrova",
                },
            },
            new ApplicationUser
            {
                UserName = "SampleUser5",
                Email = "user5@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Anatoly",
                    LastName = "Grozny",
                }
            },
            new ApplicationUser
            {
                UserName = "SampleUser6",
                Email = "user6@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Vasily",
                    LastName = "Petushkan",
                }
            },
            new ApplicationUser
            {
                UserName = "SampleUser7",
                Email = "user7@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Nadezhda",
                    LastName = "Sidorova",
                }
            },
            new ApplicationUser
            {
                UserName = "SampleUser8",
                Email = "user8@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Semyon",
                    LastName = "Grigorov",
                }
            },
            new ApplicationUser
            {
                UserName = "SampleUser9",
                Email = "user9@mail.com",
                ClientProfile = new Client
                {
                    FirstName = "Elizabeth",
                    LastName = "Kunishua",
                }
            }
        };

        public void CreateApplicationUsers()
        {
            foreach (var user in applicationUsers)
            {
                userManager.Create(user, DEFAULT_TEST_PASSWORD);
            }
        }

        public void AddUsersToRoles()
        {
            string role = applicationRoles.FirstOrDefault(r => r.Name.Equals("JustUser")).Name;
            foreach (var user in applicationUsers)
            {
                userManager.AddToRole(user.Id, role);
            }
        }

        public static IEnumerable<ApplicationUser> GetApplicationUsers() => applicationUsers;
        public static IEnumerable<ApplicationRole> GetApplicationRoles() => applicationRoles;
    }
}
