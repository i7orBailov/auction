﻿using System;
using System.Linq;
using DAL.Entities;
using System.Collections.Generic;

namespace DAL.EF
{
    public class InitHelper
    {
        static readonly Client[] clients = FillClients(IdentityInitHelper.GetApplicationUsers().ToArray());

        static readonly Condition[] conditions =
        {
            new Condition { State = "Processing" },
            new Condition { State = "Confirmed" },
            new Condition { State = "Delivered" },
        };

        static readonly ProductStatus[] productStatuses =
        {
            new ProductStatus { Title = "Waiting confirmation" },
            new ProductStatus { Title = "Accepted" },
            new ProductStatus { Title = "Sold" }
        };

        static readonly Category[] categories =
        {
            new Category 
            { 
                Title = "Smartphones",
                Image = "~/Assets/Img/phone-1.png"
            },
            new Category 
            {
                Title = "Sneakers",
                Image = "~/Assets/Img/sneakers-1.jpg"
            },
            new Category 
            { 
                Title = "Guitars",
                Image = "~/Assets/Img/guitar-1.png"
            },
        };

        static readonly GeneralProduct[] generalProducts =
        {
            new GeneralProduct
            {
                Name = "Adidas yeezy 500",
                Category = categories[1],
                Image = "~/Assets/Img/sneakers-2.jpg"
            },
            new GeneralProduct
            {
                Name = "Nike Air Max 92",
                Category = categories[1],
                Image = "~/Assets/Img/sneakers-2.jpg"
            },
            new GeneralProduct
            {
                Name = "Vans Old Skull",
                Category = categories[1],
                Image = "~/Assets/Img/sneakers-2.jpg"
            },
            new GeneralProduct
            {
                Name = "Fender XM-7 blue tango",
                Category = categories[2],
                Image = "/Assets/Img/guitar-2.png"
            },
            new GeneralProduct
            {
                Name = "Gibson sis-87 S.A.S",
                Category = categories[2],
                Image = "/Assets/Img/guitar-2.png",
            },
            new GeneralProduct
            {
                Name = "Cord B-700F black edition",
                Category = categories[2],
                Image = "/Assets/Img/guitar-2.png"
            }
        };

        static readonly ParticularProduct[] particularProducts =
        {
            new ParticularProduct
            {
                GeneralProduct = generalProducts[0],
                CurrentPrice = 380,
                Description = "The Yeezy 500 is a blend of retro and modern, with a chunky sole " +
                              "unit from the adidas archives and a high-fashion influenced upper " +
                              "constructed of mesh and premium suede.",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[0],
                CurrentPrice = 370,
                Description = "Crafted with a combination of premium suede, leather, and mesh, " +
                              "feature an adiPRENE sole, piping details, pull tab, and a rubber outsole.",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[0],
                CurrentPrice = 500,
                Description = "Features a rubber wrap along the midsole of the foot provides support and " +
                              "abrasion resistance with reflective piping details around the lace eyelets " +
                              "that add visibility in low-light conditions",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[1],
                CurrentPrice = 310,
                Description = "The design took shape via cues from the human body, " +
                              "with a lacing system inspired by a set of ribs, " +
                              "a spine-inspired outsole and a mesh and suede upper representing muscle fibres.",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[1],
                CurrentPrice = 370,
                Description = "With its unique colorway, Swoosh placement, and dual air-powered cushioning system, " +
                              "the unapologetically brash Air Max 95 quickly progressed into a youth culture icon.",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[2],
                CurrentPrice = 120,
                Description = "Vans classic skate shoe and the first to bare the iconic side stripe, " +
                              "has a low-top lace-up silhouette with a durable suede and canvas upper with " +
                              "padded tongue and lining and Vans signature Waffle Outsole.",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[2],
                CurrentPrice = 200,
                Description = "Constructed with durable suede and canvas uppers in a range of fresh colorways, " +
                              "the Old Skull pays homage to our heritage while ensuring that this low top, " +
                              "lace-up shoe remains as iconic as ever.",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[2],
                CurrentPrice = 230,
                Description = "Features re-enforced toe caps, supportive padded collars, and signature rubber waffle outsoles.",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[3],
                CurrentPrice = 700,
                Description = "Fender Musical Instruments Corporation is the world's foremost manufacturer of guitars, basses, amplifiers and related equipment.",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[3],
                CurrentPrice = 790,
                Description = "The J Mascis Telecaster® recreates J’s favorite guitar, an original 1958 top-loader Tele®",
                Status = productStatuses[1]
            },
            new ParticularProduct
            {
                GeneralProduct = generalProducts[4],
                CurrentPrice = 1100,
                Description = "Voiced to replicate the vintage Tele pickups from J’s original guitar, these custom single-coils deliver the snappy highs" +
                              " and snarly mids that set J’s amp on fire.",
                Status = productStatuses[1]
            }
        };

        public static void SetNameForEachPProduct()
        {
            int i = 1;
            foreach (var product in particularProducts)
            {
                product.Name = $"{product.GeneralProduct.Name} [{i++}]";
            }
        }

        static readonly Picture[] pictures =
        {
            new Picture
            {
                URL = "~/Assets/Img/sneakers-1.jpg",
                ParticularProduct = particularProducts[0]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-3.jpg",
                ParticularProduct = particularProducts[0]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-3.jpg",
                ParticularProduct = particularProducts[1]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-4.jpg",
                ParticularProduct = particularProducts[2]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-6.jpg",
                ParticularProduct = particularProducts[3]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-1.jpg",
                ParticularProduct = particularProducts[3]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-2.jpg",
                ParticularProduct = particularProducts[3]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-2.jpg",
                ParticularProduct = particularProducts[4]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-3.jpg",
                ParticularProduct = particularProducts[5]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-1.jpg",
                ParticularProduct = particularProducts[5]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-6.jpg",
                ParticularProduct = particularProducts[6]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-4.jpg",
                ParticularProduct = particularProducts[7]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-1.jpg",
                ParticularProduct = particularProducts[7]
            },
            new Picture
            {
                URL = "~/Assets/Img/sneakers-3.jpg",
                ParticularProduct = particularProducts[7]
            },
            new Picture
            {
                URL = "~/Assets/Img/guitar-8.jpg",
                ParticularProduct = particularProducts[8]
            },
            new Picture
            {
                URL = "~/Assets/Img/guitar-3.png",
                ParticularProduct = particularProducts[9]
            },
            new Picture
            {
                URL = "~/Assets/Img/guitar-5.png",
                ParticularProduct = particularProducts[10]
            }
        };

        //static readonly Bid[] bids =
        //{
        //    new Bid
        //    {
        //        Client = clients[2],
        //        Product = particularProducts[0],
        //        SuggestedPrice = 380
        //    },
        //    new Bid
        //    {
        //        Client = clients[3],
        //        Product = particularProducts[0],
        //        SuggestedPrice = 400
        //    },
        //    new Bid
        //    {
        //        Client = clients[4],
        //        Product = particularProducts[0],
        //        SuggestedPrice = 420
        //    },
        //    new Bid
        //    {
        //        Client = clients[5],
        //        Product = particularProducts[0],
        //        SuggestedPrice = 425
        //    },
        //    new Bid
        //    {
        //        Client = clients[6],
        //        Product = particularProducts[1],
        //        SuggestedPrice = 370
        //    },
        //    new Bid
        //    {
        //        Client = clients[7],
        //        Product = particularProducts[1],
        //        SuggestedPrice = 390
        //    },
        //    new Bid
        //    {
        //        Client = clients[8],
        //        Product = particularProducts[2],
        //        SuggestedPrice = 500
        //    },
        //    new Bid
        //    {
        //        Client = clients[3],
        //        Product = particularProducts[2],
        //        SuggestedPrice = 700
        //    }
        //};

        //static readonly Order[] orders =
        //{
        //    new Order
        //    {
        //       Bid = bids[3],
        //       ShippingAddress = "Trinkler street 10",
        //       ProcessingDate = GetRandomDate()
        //    },
        //    new Order
        //    {
        //        Bid = bids[5],
        //        ShippingAddress = "Sumska street 19/5",
        //        ProcessingDate = GetRandomDate()
        //    },
        //    new Order
        //    {
        //        Bid = bids[7],
        //        ShippingAddress = "Maselsky Ave 13a",
        //        ProcessingDate = GetRandomDate()
        //    }
        //};

        static Client[] FillClients(ApplicationUser[] applicationUsers)
        {
            Client[] clients = new Client[] { };
            foreach (var client in applicationUsers)
            {
                clients = clients.Append(client.ClientProfile).ToArray();
            }
            return clients;
        }
        //static DateTime GetRandomDate()
        //{
        //    var rnd = new Random();
        //    DateTime start = new DateTime(2020, 1, 1);
        //    int range = (DateTime.Today - start).Days;
        //    return start.AddDays(rnd.Next(maxValue: range));
        //}
        public static IEnumerable<Client> GetClients() => FillClients(IdentityInitHelper.GetApplicationUsers().ToArray());
        public static IEnumerable<Condition> GetConditions() => conditions;
        public static IEnumerable<Category> GetCategories() => categories;
        public static IEnumerable<GeneralProduct> GetGeneralProducts() => generalProducts;
        public static IEnumerable<ParticularProduct> GetParticularProducts() => particularProducts;
        public static IEnumerable<ProductStatus> GetProductStatuses() => productStatuses;
        public static IEnumerable<Picture> GetPictures() => pictures;
        //public static IEnumerable<Bid> GetBids() => bids;
        //public static IEnumerable<Order> GetOrders() => orders;
    }
}
