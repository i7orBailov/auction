﻿using System;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IEFUnitOfWork : IDisposable
    {
        bool Exists { get; }
        IEFGenericRepository<Bid> Bids { get; }
        IEFGenericRepository<Order> Orders { get; }
        IEFGenericRepository<Client> Clients { get; }
        IEFGenericRepository<Picture> Pictures { get; }
        IEFGenericRepository<Category> Categories { get; }
        IEFGenericRepository<Condition> Conditions { get; }
        IEFGenericRepository<ProductStatus> ProductStatuses { get; }
        IEFGenericRepository<GeneralProduct> GeneralProducts { get; }
        IEFGenericRepository<ParticularProduct> ParticularProducts { get; }
    }
}
