﻿using System;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IExceptionUnitOfWork : IDisposable
    {
        IEFGenericRepository<ExceptionDetail> Exceptions { get; }
    }
}
