﻿using System;
using DAL.Identity;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IIdentityUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
        IClientManager ClientManager { get; }
        Task SaveAsync();
    }
}
