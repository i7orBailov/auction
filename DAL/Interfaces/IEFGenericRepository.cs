﻿using System;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace DAL.Interfaces
{
    public interface IEFGenericRepository<TEntity> where TEntity : class
    {
        Task CreateAsync(TEntity item);
        Task ModifyAsync(TEntity item);
        Task RemoveAsync(TEntity item);
        Task<TEntity> FindByIdAsync(int? id);
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<IEnumerable<TEntity>> GetByConditionAsync(Expression<Func<TEntity, bool>> predicate);

        void Create(TEntity item);
        void Modify(TEntity item);
        void Remove(TEntity item);
        TEntity FindById(int? id);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetByCondition(Func<TEntity, bool> predicate);
    }
}
