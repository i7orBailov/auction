﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStatusToProduct : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductStatus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ParticularProducts", "Status_Id", c => c.Int());
            CreateIndex("dbo.ParticularProducts", "Status_Id");
            AddForeignKey("dbo.ParticularProducts", "Status_Id", "dbo.ProductStatus", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParticularProducts", "Status_Id", "dbo.ProductStatus");
            DropIndex("dbo.ParticularProducts", new[] { "Status_Id" });
            DropColumn("dbo.ParticularProducts", "Status_Id");
            DropTable("dbo.ProductStatus");
        }
    }
}
