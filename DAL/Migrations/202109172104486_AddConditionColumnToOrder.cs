﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddConditionColumnToOrder : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Condition_Id", "dbo.Conditions");
            DropIndex("dbo.Orders", new[] { "Condition_Id" });
            RenameColumn(table: "dbo.Orders", name: "Condition_Id", newName: "ConditionId");
            AlterColumn("dbo.Orders", "ConditionId", c => c.Int(nullable: false));
            CreateIndex("dbo.Orders", "ConditionId");
            AddForeignKey("dbo.Orders", "ConditionId", "dbo.Conditions", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "ConditionId", "dbo.Conditions");
            DropIndex("dbo.Orders", new[] { "ConditionId" });
            AlterColumn("dbo.Orders", "ConditionId", c => c.Int());
            RenameColumn(table: "dbo.Orders", name: "ConditionId", newName: "Condition_Id");
            CreateIndex("dbo.Orders", "Condition_Id");
            AddForeignKey("dbo.Orders", "Condition_Id", "dbo.Conditions", "Id");
        }
    }
}
