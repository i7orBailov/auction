﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterBidTableaddClientReference : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Orders", name: "Client_Id", newName: "ClientId");
            RenameIndex(table: "dbo.Orders", name: "IX_Client_Id", newName: "IX_ClientId");
            DropColumn("dbo.Orders", "ShippingAddress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "ShippingAddress", c => c.String());
            RenameIndex(table: "dbo.Orders", name: "IX_ClientId", newName: "IX_Client_Id");
            RenameColumn(table: "dbo.Orders", name: "ClientId", newName: "Client_Id");
        }
    }
}
