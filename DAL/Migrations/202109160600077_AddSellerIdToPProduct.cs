﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSellerIdToPProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ParticularProducts", "SellerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.ParticularProducts", "SellerId");
            AddForeignKey("dbo.ParticularProducts", "SellerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ParticularProducts", "SellerId", "dbo.AspNetUsers");
            DropIndex("dbo.ParticularProducts", new[] { "SellerId" });
            DropColumn("dbo.ParticularProducts", "SellerId");
        }
    }
}
