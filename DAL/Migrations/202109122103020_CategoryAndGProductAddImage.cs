﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryAndGProductAddImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GeneralProducts", "Image", c => c.String());
            AddColumn("dbo.Categories", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Categories", "Image");
            DropColumn("dbo.GeneralProducts", "Image");
        }
    }
}
