﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestRemoveDatetime2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bids", "ExpirationDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Orders", "ProcessingDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "ProcessingDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Bids", "ExpirationDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
    }
}
