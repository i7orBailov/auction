﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStatusIdReferenceToPProduct : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ParticularProducts", name: "Status_Id", newName: "StatusId");
            RenameIndex(table: "dbo.ParticularProducts", name: "IX_Status_Id", newName: "IX_StatusId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ParticularProducts", name: "IX_StatusId", newName: "IX_Status_Id");
            RenameColumn(table: "dbo.ParticularProducts", name: "StatusId", newName: "Status_Id");
        }
    }
}
