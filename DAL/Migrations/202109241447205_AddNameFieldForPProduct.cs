﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNameFieldForPProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ParticularProducts", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ParticularProducts", "Name");
        }
    }
}
