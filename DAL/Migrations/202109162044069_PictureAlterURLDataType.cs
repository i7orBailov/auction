﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PictureAlterURLDataType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pictures", "URL", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pictures", "URL", c => c.Binary());
        }
    }
}
