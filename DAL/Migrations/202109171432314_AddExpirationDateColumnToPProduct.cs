﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddExpirationDateColumnToPProduct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ParticularProducts", "ExpirationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ParticularProducts", "ExpirationDate");
        }
    }
}
