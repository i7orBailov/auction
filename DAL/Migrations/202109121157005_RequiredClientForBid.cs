﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredClientForBid : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Bids", "ClientId", "dbo.Clients");
            DropIndex("dbo.Bids", new[] { "ClientId" });
            AlterColumn("dbo.Bids", "ClientId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Bids", "ClientId");
            AddForeignKey("dbo.Bids", "ClientId", "dbo.Clients", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bids", "ClientId", "dbo.Clients");
            DropIndex("dbo.Bids", new[] { "ClientId" });
            AlterColumn("dbo.Bids", "ClientId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Bids", "ClientId");
            AddForeignKey("dbo.Bids", "ClientId", "dbo.Clients", "Id");
        }
    }
}
