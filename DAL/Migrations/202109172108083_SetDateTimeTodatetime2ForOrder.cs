﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetDateTimeTodatetime2ForOrder : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Orders", "ProcessingDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "ProcessingDate", c => c.DateTime(nullable: false));
        }
    }
}
