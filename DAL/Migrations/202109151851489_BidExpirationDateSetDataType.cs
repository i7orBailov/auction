﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BidExpirationDateSetDataType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bids", "ExpirationDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Bids", "ExpirationDate", c => c.DateTime(nullable: false));
        }
    }
}
