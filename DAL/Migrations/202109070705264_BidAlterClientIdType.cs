﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BidAlterClientIdType : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Bids", new[] { "Client_Id" });
            DropColumn("dbo.Bids", "ClientId");
            RenameColumn(table: "dbo.Bids", name: "Client_Id", newName: "ClientId");
            AlterColumn("dbo.Bids", "ClientId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Bids", "ClientId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Bids", new[] { "ClientId" });
            AlterColumn("dbo.Bids", "ClientId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Bids", name: "ClientId", newName: "Client_Id");
            AddColumn("dbo.Bids", "ClientId", c => c.Int(nullable: false));
            CreateIndex("dbo.Bids", "Client_Id");
        }
    }
}
