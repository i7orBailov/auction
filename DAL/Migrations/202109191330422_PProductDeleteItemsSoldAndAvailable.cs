﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PProductDeleteItemsSoldAndAvailable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ParticularProducts", "ItemsSold");
            DropColumn("dbo.ParticularProducts", "ItemsAvailable");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ParticularProducts", "ItemsAvailable", c => c.Int());
            AddColumn("dbo.ParticularProducts", "ItemsSold", c => c.Int());
        }
    }
}
