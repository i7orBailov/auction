﻿using DAL.EF;
using System;
using DAL.Entities;
using DAL.Identity;
using DAL.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Repositories
{
    public class EFUnitOfWork : IEFUnitOfWork, IExceptionUnitOfWork, IIdentityUnitOfWork
    {
        readonly AuctionContext db;
        readonly IClientManager clientManager;
        readonly ApplicationUserManager userManager;
        readonly ApplicationRoleManager roleManager;

        IEFGenericRepository<Bid> bids;
        IEFGenericRepository<Order> orders;
        IEFGenericRepository<Client> clients;
        IEFGenericRepository<Picture> pictures;
        IEFGenericRepository<Category> categories;
        IEFGenericRepository<Condition> conditions;
        IEFGenericRepository<ProductStatus> productStatuses;
        IEFGenericRepository<GeneralProduct> generalProducts;
        IEFGenericRepository<ParticularProduct> particularProducts;
        
        IEFGenericRepository<ExceptionDetail> exceptions;

        public EFUnitOfWork(AuctionContext _db)
        {
            db = _db;
        }

        public EFUnitOfWork()
        {
            db = new AuctionContext();
            clientManager = new ClientManager(db);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
        }

        #region EF UOW
        public IEFGenericRepository<Bid> Bids
        {
            get
            {
                if (bids is null)
                    bids = new EFGenericRepository<Bid>(db);
                return bids;
            }
        }
        public IEFGenericRepository<Order> Orders
        {
            get
            {
                if (orders is null)
                    orders = new EFGenericRepository<Order>(db);
                return orders;
            }
        }
        public IEFGenericRepository<Client> Clients
        {
            get
            {
                if (clients is null)
                    clients = new EFGenericRepository<Client>(db);
                return clients;
            }
        }
        public IEFGenericRepository<Picture> Pictures
        {
            get
            {
                if (pictures is null)
                    pictures = new EFGenericRepository<Picture>(db);
                return pictures;
            }
        }
        public IEFGenericRepository<Category> Categories
        {
            get
            {
                if (categories is null)
                    categories = new EFGenericRepository<Category>(db);
                return categories;
            }
        }
        public IEFGenericRepository<Condition> Conditions
        {
            get
            {
                if (conditions is null)
                    conditions = new EFGenericRepository<Condition>(db);
                return conditions;
            }
        }
        public IEFGenericRepository<ProductStatus> ProductStatuses
        {
            get
            {
                if (productStatuses is null)
                    productStatuses = new EFGenericRepository<ProductStatus>(db);
                return productStatuses;
            }
        }
        public IEFGenericRepository<GeneralProduct> GeneralProducts
        {
            get
            {
                if (generalProducts is null)
                    generalProducts = new EFGenericRepository<GeneralProduct>(db);
                return generalProducts;
            }
        }  
        public IEFGenericRepository<ParticularProduct> ParticularProducts
        {
            get
            {
                if (particularProducts is null)
                    particularProducts = new EFGenericRepository<ParticularProduct>(db);
                return particularProducts;
            }
        }
        #endregion

        #region Exception UOW
        public IEFGenericRepository<ExceptionDetail> Exceptions
        {
            get
            {
                if (exceptions is null)
                    exceptions = new EFGenericRepository<ExceptionDetail>(db);
                return exceptions;
            }
        }
        #endregion

        #region Identity UOW
        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public IClientManager ClientManager
        {
            get { return clientManager; }
        }

        public async Task SaveAsync() => await db.SaveChangesAsync();
        #endregion

        public bool Exists { get { return db.Exists; } }

        bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
