﻿using System;
using DAL.EF;
using System.Linq;
using DAL.Interfaces;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace DAL.Repositories
{
    public class EFGenericRepository<TEntity> : IEFGenericRepository<TEntity> where TEntity : class
    {
        readonly DbContext _context;
        readonly DbSet<TEntity> _dbSet;

        public EFGenericRepository(AuctionContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public EFGenericRepository()
        {
            _context = new AuctionContext();
            _dbSet = _context.Set<TEntity>();
        }

        #region asynchronous
        public async Task CreateAsync(TEntity item)
        {
            if (!_dbSet.Local.Contains(item))
                _dbSet.Attach(item); 

            _dbSet.Add(item);
            await _context.SaveChangesAsync();
        }

        public async Task ModifyAsync(TEntity item)
        {
            _dbSet.AddOrUpdate(item);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(TEntity item)
        {
            if (!_dbSet.Local.Contains(item))
                _dbSet.Attach(item);

            _dbSet.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task<TEntity> FindByIdAsync(int? id) => await _dbSet.FindAsync(id);

        public async Task<IEnumerable<TEntity>> GetAllAsync() => await _dbSet.AsNoTracking().ToListAsync();
        
        public async Task<IEnumerable<TEntity>> GetByConditionAsync(Expression<Func<TEntity, bool>> predicate) =>
            await _dbSet.AsNoTracking().Where(predicate).ToListAsync();
        #endregion

        #region synchronous
        public void Create(TEntity item)
        {
            if (!_dbSet.Local.Contains(item))
                _dbSet.Attach(item);

            _dbSet.Add(item);
            _context.SaveChanges();
        }

        public void Modify(TEntity item)
        {
            _dbSet.AddOrUpdate(item);
            _context.SaveChanges();
        }

        public void Remove(TEntity item)
        {
            if (!_dbSet.Local.Contains(item))
                _dbSet.Attach(item);

            _dbSet.Remove(item);
            _context.SaveChanges();
        }

        public TEntity FindById(int? id) => _dbSet.Find(id);

        public IEnumerable<TEntity> GetAll() => _dbSet.AsNoTracking().ToList();

        public IEnumerable<TEntity> GetByCondition(Func<TEntity, bool> predicate) =>
            _dbSet.AsNoTracking().Where(predicate).ToList();
        #endregion
    }
}
