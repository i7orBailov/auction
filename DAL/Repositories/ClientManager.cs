﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories
{
    public class ClientManager : IClientManager
    {
        public AuctionContext Database { get; set; }

        public ClientManager(AuctionContext db)
        {
            Database = db;
        }

        public void Create(Client item)
        {
            Database.Clients.Add(item);
            Database.SaveChanges();
        }

        public void Dispose() => Database.Dispose();
    }
}
