﻿using System;
using BLL.DTO;
using BLL.Services;
using System.Web.Mvc;
using System.Diagnostics;

namespace PL.Filters
{
    public class CustomExceptionHandlerAttribute : FilterAttribute, IExceptionFilter
    {
        readonly ExceptionService _service = new ExceptionService();

        public void OnException(ExceptionContext filterContext)
        {
            ExceptionDetailDTO exceptionDetailDto = new ExceptionDetailDTO()
            {
                Date = DateTime.Now,
                StackTrace = filterContext.Exception.StackTrace,
                ExceptionMessage = filterContext.Exception.Message,
                ActionName = filterContext.RouteData.Values["action"].ToString(),
                ControllerName = filterContext.RouteData.Values["controller"].ToString()
            };

            filterContext.Result = new ViewResult()
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary<string>(filterContext.Exception.Message),
            };
            _service.SaveException(exceptionDetailDto);
            Debug.WriteLine(exceptionDetailDto.ExceptionMessage);
            
            filterContext.ExceptionHandled = true;
        }
    }
}