﻿using System.Web;
using System.Linq;
using BLL.Services;
using BLL.Interfaces;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;

namespace PL.Filters
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        string status;
        const string UNAUTHORIZED = "unauthorized";
        const string NOT_SET_USER_INFO = "not set user info";
        const string INSUFFICIENT_RIGHTS = "insuficcient rights";

        readonly bool checkPersonalInfo;
        readonly IUserBackgroundService _service;
        string[] allowedUsers = new string[] { };
        string[] allowedRoles = new string[] { };

        public CustomAuthorizeAttribute(bool checkPersonalInfo = true)
        {
            _service = new UserBackgroundService();
            this.checkPersonalInfo = checkPersonalInfo;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!string.IsNullOrEmpty(base.Users))
            {
                allowedUsers = base.Users.Split(new char[] { ',' });
                for (int i = 0; i < allowedUsers.Length; i++)
                {
                    allowedUsers[i] = allowedUsers[i].Trim();
                }
            }
            if (!string.IsNullOrEmpty(base.Roles))
            {
                allowedRoles = base.Roles.Split(new char[] { ',' });
                for (int i = 0; i < allowedRoles.Length; i++)
                {
                    allowedRoles[i] = allowedRoles[i].Trim();
                }
            }

            bool result = httpContext.Request.IsAuthenticated &&
                          User(httpContext) && Role(httpContext);

            SetStatus(httpContext);
            return checkPersonalInfo ? result && HasPersonalInfo(httpContext) : result;
        }

        void SetStatus(HttpContextBase httpContext) 
        {
            if (!httpContext.Request.IsAuthenticated)
                status = UNAUTHORIZED;
            else if (checkPersonalInfo)
            {
                if (!HasPersonalInfo(httpContext))
                    status = NOT_SET_USER_INFO;
            }
            else if (!Role(httpContext))
                status = INSUFFICIENT_RIGHTS;
        }

        bool User(HttpContextBase httpContext)
        {
            if (allowedUsers.Length > 0)
                return allowedUsers.Contains(httpContext.User.Identity.Name);
            return true;
        }

        bool Role(HttpContextBase httpContext)
        {
            if (allowedRoles.Length > 0)
            {
                for (int i = 0; i < allowedRoles.Length; i++)
                {
                    if (httpContext.User.IsInRole(allowedRoles[i]))
                        return true;
                }
                return false;
            }
            return true;
        }

        bool HasPersonalInfo(HttpContextBase httpContext) =>
            _service.HasPersonalInfo(httpContext.User.Identity.GetUserId());

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (status == INSUFFICIENT_RIGHTS)
                filterContext.Result = new RedirectToRouteResult(
                   new RouteValueDictionary
                   {
                        { "controller", "Error" },
                        { "action", "Forbidden" }
                   });
            else if (status == NOT_SET_USER_INFO)
                filterContext.Result = new RedirectToRouteResult(
                   new RouteValueDictionary
                   {
                        { "controller", "Error" },
                        { "action", "NoPersonalInfo" }
                   });
            else if (status == UNAUTHORIZED)
                filterContext.Result = new RedirectToRouteResult(
                   new RouteValueDictionary
                   {
                        { "controller", "Error" },
                        { "action", "NotLogged" }
                   });
        }
    }
}