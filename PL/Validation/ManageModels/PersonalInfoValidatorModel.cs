﻿using PL.Models;
using FluentValidation;

namespace PL.Validation.ManageModels
{
    public class PersonalInfoValidatorModel : AbstractValidator<PersonalInfoViewModel>
    {
        const string REQUIRED = "Required field";
        const string INCORRECT_EMAIL = "Incorrect email";
        const string INCORRECT_NAME = "This field should contain no spaces and number characters";

        public PersonalInfoValidatorModel()
        {
            RuleFor(m => m.FirstName).NotEmpty()
                                     .WithMessage(REQUIRED)
                                     .Matches(@"^[^0-9\s]+$")
                                     .WithMessage(INCORRECT_NAME);

            RuleFor(m => m.LastName).NotEmpty()
                                    .WithMessage(REQUIRED)
                                    .Matches(@"^[^0-9\s]+$")
                                    .WithMessage(INCORRECT_NAME);

            RuleFor(m => m.Email).NotEmpty()
                                 .WithMessage(REQUIRED)
                                 .EmailAddress()
                                 .WithMessage(INCORRECT_EMAIL);

            RuleFor(m => m.Address).NotEmpty()
                                   .WithMessage(REQUIRED);
        }
    }
}