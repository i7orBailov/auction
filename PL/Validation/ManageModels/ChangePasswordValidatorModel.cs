﻿using PL.Models;
using FluentValidation;

namespace PL.Validation.ManageModels
{
    public class ChangePasswordValidatorModel : AbstractValidator<ChangePasswordViewModel>
    {
        public ChangePasswordValidatorModel()
        {
            const string REQUIRED = "Required field";
            const string PASS_EQUAL = "Passwords should be equal";
            const string INCORRECT_PASS = "Password should have at least one lowercase, " +
                                          "uppercase letter, number and any other character." +
                                          "Should not be less than 6 neither more than 15 symbols";

            RuleFor(m => m.OldPassword).NotEmpty()
                                       .WithMessage(REQUIRED);

            RuleFor(m => m.NewPassword).NotEmpty()
                                       .WithMessage(REQUIRED)
                                       .Matches(@"^(?=.*[A-Z])" +
                                               "(?=.*[!@#$&*])" +
                                               "(?=.*[0-9])" +
                                               "(?=.*[a-z])" +
                                               ".{6,15}$")
                                       .WithMessage(INCORRECT_PASS);

            RuleFor(m => m.ConfirmPassword).NotEmpty()
                                           .WithMessage(REQUIRED)
                                           .Equal(p => p.NewPassword)
                                           .WithMessage(PASS_EQUAL);
        }
    }
}