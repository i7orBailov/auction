﻿using System;
using PL.Models;
using FluentValidation;

namespace PL.Validation.ProductModels
{
    public class SellNewProductValidatorModel : AbstractValidator<SellNewProductViewModel>
    {
        const string REQUIRED = "Required field";
        const string INCORRECT_IMAGE = "File should be only image.";
        const string PRICE_RANGE = "Price can`t be less than 1 neither more than 500 000";
        const string DATE_RANGE = "Date can be neither less than current one nor long more than 1 month.";

        public SellNewProductValidatorModel()
        {
            RuleFor(m => m.Name).NotEmpty()
                                .WithMessage(REQUIRED);

            RuleFor(m => m.Description).NotEmpty()
                                       .WithMessage(REQUIRED);

            RuleFor(m => m.InitialPrice).NotEmpty()
                                        .WithMessage(REQUIRED)
                                        .InclusiveBetween(from: 1, to: 500000)
                                        .WithMessage(PRICE_RANGE);

            RuleFor(m => m.ExpirationDate).NotEmpty()
                                          .WithMessage(REQUIRED)
                                          .InclusiveBetween(from: DateTime.Now, to: DateTime.Now.AddMonths(1))
                                          .WithMessage(DATE_RANGE);

            RuleFor(m => m.Image.FileName).Matches(@"[Ёёа-яА-Яa-zA-Z\s\d-.]*.(jpg|jpeg|png)$")
                                          .WithMessage(INCORRECT_IMAGE)
                                          .When(m => m.Image != null);
        }
    }
}