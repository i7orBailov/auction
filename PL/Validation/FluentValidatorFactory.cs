﻿using System;
using PL.Models;
using FluentValidation;
using PL.Validation.BidModels;
using PL.Validation.ManageModels;
using System.Collections.Generic;
using PL.Validation.ProductModels;
using PL.Validation.AuthorizationModels;

namespace PL.Validation
{
    public class FluentValidatorFactory : ValidatorFactoryBase
    {
        static readonly Dictionary<Type, IValidator> validators = new Dictionary<Type, IValidator>();

        static FluentValidatorFactory()
        {
            validators.Add(typeof(IValidator<SellNewProductViewModel>), new SellNewProductValidatorModel());
            validators.Add(typeof(IValidator<MakeBidViewModel>), new MakeBidValidatorModel());
            validators.Add(typeof(IValidator<LoginViewModel>), new LoginValidatorModel());
            validators.Add(typeof(IValidator<RegisterViewModel>), new RegisterValidatorModel());
            validators.Add(typeof(IValidator<ChangePasswordViewModel>), new ChangePasswordValidatorModel());
            validators.Add(typeof(IValidator<PersonalInfoViewModel>), new PersonalInfoValidatorModel());
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            if (validators.TryGetValue(validatorType, out IValidator validator))
            {
                return validator;
            }
            return validator;
        }
    }
}