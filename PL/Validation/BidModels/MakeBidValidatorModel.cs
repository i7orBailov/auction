﻿using PL.Models;
using FluentValidation;

namespace PL.Validation.BidModels
{
    public class MakeBidValidatorModel : AbstractValidator<MakeBidViewModel>
    {
        const string REQUIRED = "Required field";
        const string INCORRECT_PRICE = "New bid price should not be less than initial price";
        const string NOT_EQUAL_CUSTOMER_ID = "You can not make more than one bid in a row! " +
                                             "Wait till someone else makes new one";
        const string NOT_EQUAL_OWNER_ID = "It is forbidden to make a bid on product one owns";

        public MakeBidValidatorModel()
        {
            RuleFor(m => m.SuggestedPrice).NotEmpty()
                                          .WithMessage(REQUIRED)
                                          .GreaterThan(sp => sp.InitialPrice)
                                          .WithMessage(INCORRECT_PRICE);

            RuleFor(m => m.ClientId).NotEqual(c => c.BidLastCustomerId)
                                    .WithMessage(NOT_EQUAL_CUSTOMER_ID);
            
            RuleFor(m => m.ClientId).NotEqual(c => c.ProductOwnerId)
                                    .WithMessage(NOT_EQUAL_OWNER_ID);
        }
    }
}