﻿using PL.Models;
using FluentValidation;

namespace PL.Validation.AuthorizationModels
{
    public class LoginValidatorModel : AbstractValidator<LoginViewModel>
    {
        const string REQUIRED = "Both fields are required";

        public LoginValidatorModel()
        {
            RuleFor(m => m.NickName).NotEmpty()
                                    .WithMessage(REQUIRED);

            RuleFor(m => m.Password).NotEmpty()
                                    .WithMessage(REQUIRED);
        }
    }
}