﻿using PL.Models;
using FluentValidation;

namespace PL.Validation.AuthorizationModels
{
    public class RegisterValidatorModel : AbstractValidator<RegisterViewModel>
    {
        const string REQUIRED = "Required field";
        const string PASS_EQUAL = "Passwords should be equal";
        const string INCORRECT_NAME = "Number can not be first. Latin alphabet and letters with no spaces only";
        const string INCORRECT_PASS = "Password should have at least one lowercase, " +
                                      "uppercase letter, number and any other character." + 
                                      "Should not be less than 6 neither more than 15 symbols";

        public RegisterValidatorModel()
        {
            RuleFor(m => m.NickName).NotEmpty()
                                    .WithMessage(REQUIRED)
                                    .Matches(@"^\D\w{0,}")
                                    .WithMessage(INCORRECT_NAME);

            RuleFor(m => m.Password).NotEmpty()
                                    .WithMessage(REQUIRED)
                                    .Matches(@"^(?=.*[A-Z])" +
                                               "(?=.*[!@#$&*])" +
                                               "(?=.*[0-9])" +
                                               "(?=.*[a-z])" +
                                               ".{6,}$")
                                    .WithMessage(INCORRECT_PASS);

            RuleFor(m => m.ConfirmPassword).NotEmpty()
                                           .WithMessage(REQUIRED)
                                           .Equal(p => p.Password)
                                           .WithMessage(PASS_EQUAL);
        }
    }
}