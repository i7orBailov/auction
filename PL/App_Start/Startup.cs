﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using BLL.Services;
using Microsoft.AspNet.Identity;
using BLL.Interfaces;

[assembly: OwinStartup(typeof(PL.App_Start.Startup))]

namespace PL.App_Start
{
    public class Startup
    {
        readonly IServiceCreator serviceCreator = new ServiceCreator();
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(CreateUserService);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
        }

        IUserService CreateUserService() => serviceCreator.CreateUserService();
    }
}