﻿using System;
using BLL.Services;
using PL.App_Start;
using System.Diagnostics;
using System.Web.Hosting;
using System.Threading;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(BackgroundWorker), "Start")]

namespace PL.App_Start
{
    public static class BackgroundWorker
    {
        static readonly JobHost _jobHost = new JobHost();
        static readonly OrderService service = new OrderService();
        static readonly Thread thread = new Thread(async () => await service.AutoGenerateOrderAsync());

        public static void Start() => 
            _jobHost.DoWork(() => 
            { 
                Debug.WriteLine("Background work started");
                thread.Start();
            });
    }

    public class JobHost : IRegisteredObject
    {
        readonly object _lock = new object();
        bool _shuttingDown;

        public JobHost()
        {
            HostingEnvironment.RegisterObject(this);
        }

        public void Stop(bool immediate)
        {
            lock (_lock)
            {
                _shuttingDown = true;
            }
            HostingEnvironment.UnregisterObject(this);
        }

        public void DoWork(Action work)
        {
            lock (_lock)
            {
                if (_shuttingDown)
                {
                    return;
                }
                work();
            }
        }
    }
}