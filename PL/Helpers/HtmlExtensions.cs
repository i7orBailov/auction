﻿using System.Web.Mvc;

namespace PL.Helpers
{
    public static class HtmlExtensions
    {
        static readonly string DEFAULT_VALUE = "Not set";

        public static string ShowAbout(this HtmlHelper helper, string model) =>
            string.Format($"{model ?? DEFAULT_VALUE}");
    }
}