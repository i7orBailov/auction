﻿using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;

namespace PL.Helpers
{
    public class ErrorHelper
    {
        public static IEnumerable<string> GetErrorList(ICollection<ModelState> states)
        {
            var errorList = new List<string>();
            foreach (var error in states.Where(m => m.Errors.Count() > 0))
            {
                error.Errors.Select(x => x.ErrorMessage).ToList().ForEach(x => errorList.Add(x));
            }
            return errorList;
        }
    }
}