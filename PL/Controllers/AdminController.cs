﻿using BLL.DTO;
using PL.Models;
using PL.Filters;
using AutoMapper;
using BLL.Interfaces;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using BLL.Interfaces.AdminInterfaces;

namespace PL.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        readonly IMapper _mapper;
        readonly IAdminUserService _userService;
        readonly IAdminProductService _productService;

        public AdminController(IMapper mapper, IAdminProductService productService, IAdminUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
            _productService = productService;
        }

        #region Products
        public async Task<ActionResult> ProductsWaitingForConfirmation()
        {
            var productsDto = await _productService.GetProductsWaitingForConfirmationAsync();
            var products = _mapper.Map<IEnumerable<ParticularProductViewModel>>(productsDto);
            return View(products);
        }

        public async Task<ActionResult> ProductWaitingForConfirmation(int? id)
        {
            var productDto = await _productService.GetProductWaitingForConfirmationAsync(id);
            var product = _mapper.Map<ParticularProductViewModel>(productDto);
            return View(product);
        }

        public async Task<ActionResult> AcceptRequestForProductSale(int? id)
        {
            await _productService.AcceptRequestForProductSaleAsync(id);
            return RedirectToAction(nameof(ProductsWaitingForConfirmation));
        }

        public async Task<ActionResult> DeclineRequestForProductSale(int? id)
        {
            await _productService.DeclineRequestForProductSaleAsync(id);
            return RedirectToAction(nameof(ProductsWaitingForConfirmation));
        }

        public async Task<ActionResult> DeleteProduct(int? id)
        {
            await _productService.DeleteProductAsync(id);
            return RedirectToAction(nameof(ProductDeletedSuccessfully));
        }

        public ActionResult ProductDeletedSuccessfully() => View();
        #endregion

        public async Task<ActionResult> Users()
        {
            var usersDto = await _userService.GetAllUsersAsync();
            var model = _mapper.Map<IEnumerable<ClientDTO>, IEnumerable<CustomerViewModel>>(usersDto);
            return View(model);
        }

        public async Task<ActionResult> PromoteUser()
        {
            string userId = TempData["userId"] as string;
            var user = await _userService.PromoteAndGetUserAsync(userId);
            return View(model: user.NickName);
        }
    }
}