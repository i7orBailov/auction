﻿using BLL.DTO;
using PL.Models;
using PL.Filters;
using AutoMapper;
using BLL.Interfaces;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;

namespace PL.Controllers
{
    [CustomAuthorize]
    public class OrderController : Controller
    {
        readonly IMapper _mapper;
        readonly IOrderService _service;
        const string USER_ID = "User id";

        public OrderController(IOrderService service, IMapper mapper)
        {
            _mapper = mapper;
            _service = service;
        }

        public async Task<ActionResult> Show()
        {
            string userId = User.Identity.GetUserId();
            var ordersDto = await _service.GetAllOrdersAsync(userId);
            var model = _mapper.Map<IEnumerable<ShowOrderDTO>, IEnumerable<ShowOrderViewModel>>(ordersDto);
            return View(model);
        }

        public async Task<ActionResult> Remove(int? id)
        {
            string userId = TempData[nameof(USER_ID)] as string;
            await _service.RemoveOrder(id, userId);
            return RedirectToAction(nameof(Show));
        }

        public async Task<ActionResult> Accept(int? id)
        {
            string userId = TempData[nameof(USER_ID)] as string;
            await _service.AcceptOrder(id, userId);
            return View();
        }
    }
}