﻿using BLL.DTO;
using PL.Models;
using PL.Helpers;
using PL.Filters;
using AutoMapper;
using BLL.Interfaces;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [CustomAuthorize]
    public class BidController : Controller
    {
        readonly IMapper _mapper;
        const string ERRORS = "errors";
        readonly IBidService _bidService;
        readonly IProductService _productService;

        public BidController(IBidService serv, IMapper mapper, IProductService pServ)
        {
            _mapper = mapper;
            _bidService = serv;
            _productService = pServ;
        }

        public async Task<ActionResult> MakeBid(int? id)
        {
            var initialPrice = await _bidService.GetProductInitialPriceAsync(id);
            var lastCustomerId = await _bidService.GetLastCustomerIdAsync(id);
            var ownerId = await _bidService.GetProductOwnerId(id);
            var model = new MakeBidViewModel()
            {
                ProductId = id ?? 0,
                InitialPrice = initialPrice,
                BidLastCustomerId = lastCustomerId,
                ProductOwnerId = ownerId
            };
            return View(model);
        }

        public async Task<ActionResult> ConfirmBid(MakeBidViewModel bid)
        {
            if (ModelState.IsValid)
            {
                var product = await _productService.GetParticularProductByIdAsync(bid.ProductId);
                var generalProduct = await _productService.GetGeneralProductByIdAsync(product.GeneralProductId);
                var bidProduct = new BidProductViewModel
                {
                    CategoryTitle = generalProduct.Category.Title,
                    InitialPrice = product.CurrentPrice,
                    ProductDescription = product.Description,
                    ProductName = generalProduct.Name,
                    SuggesterPrice = bid.SuggestedPrice,
                    Id = bid.ProductId
                };
                TempData[nameof(bid)] = bid;
                return View(bidProduct);
            }
            TempData[nameof(ERRORS)] = ErrorHelper.GetErrorList(ModelState.Values);
            return RedirectToAction(nameof(MakeBid), new { id = bid.ProductId });
        }

        public async Task<ActionResult> SaveBid(MakeBidViewModel bid)
        {
            bid = TempData[nameof(bid)] as MakeBidViewModel ?? bid;
            var bidDto = _mapper.Map<BidDTO>(bid);
            await _bidService.MakeBidAsync(bidDto);
            return View();
        }
    }
}