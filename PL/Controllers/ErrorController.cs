﻿using System.Web.Mvc;

namespace PL.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index() => View();
        
        public ActionResult Forbidden() => View();

        public ActionResult NotLogged() => View();

        public ActionResult NoPersonalInfo() => View();
        
        public ActionResult TooLarge() => View();

        public ActionResult BadRequest() => View();

        public ActionResult NotFound() => View();

        public ActionResult Internal() => View();
    }
}