﻿ using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PL.Helpers;
using PL.Models;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
    public class AccountController : Controller
    {
        const string ERRORS = "errors";
        IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Login() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                ClaimsIdentity userClaim = await GetUserClaimAsync(model.Password, model.NickName);
                if (userClaim is null)
                    ModelState.AddModelError(string.Empty, "Incorrect login or password.");
                else
                {
                    SignUserIn(userClaim);
                    return RedirectToAction("Index", "Home");
                }
            }
            TempData[nameof(ERRORS)] = ErrorHelper.GetErrorList(ModelState.Values);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                string userRole = UserService.GetRoleOfUser();
                ClientDTO userDto = new ClientDTO
                {
                    Password = model.Password,
                    NickName = model.NickName,
                    Role = userRole
                };
                OperationDetails operationDetails = await UserService.CreateAsync(userDto);
                if (operationDetails.Succedeed)
                {
                    SignUserIn(await GetUserClaimAsync(userDto.Password, userDto.NickName));
                    return RedirectToAction("Index", "Home");
                }
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            TempData[nameof(ERRORS)] = ErrorHelper.GetErrorList(ModelState.Values);
            return View(model);
        }

        async Task<ClaimsIdentity> GetUserClaimAsync(string password, string nickName)
        {
            ClientDTO userDto = new ClientDTO
            {
                NickName = nickName,
                Password = password
            };
            return await UserService.AuthenticateAsync(userDto);
        }

        void SignUserIn(ClaimsIdentity claim)
        {
            AuthenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = true
            }, claim);
        }
    }
}