﻿using BLL.DTO;
using PL.Models;
using AutoMapper;
using BLL.Interfaces;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Internet_Auction.PL.Controllers
{
    public class HomeController : Controller
    {
        readonly IMapper _mapper;
        readonly IProductService _productService;

        public HomeController(IProductService serv, IMapper mapper)
        {
            _mapper = mapper;
            _productService = serv;
        }

        public async Task<ActionResult> Index()
        {
            IEnumerable<CategoryDTO> categoryDtos = await _productService.GetCategoriesAsync();
            var categories = _mapper.Map<IEnumerable<CategoryDTO>, List<CategoryViewModel>>(categoryDtos);
            return View(categories);
        }

        public ActionResult Search() => View();

        public async Task<ActionResult> Find(string name)
        {
            var particularProductsDto = await _productService.GetParticularProductListAsync(name);
            var products = _mapper.Map<IEnumerable<ParticularProductDTO>, IEnumerable<ParticularProductViewModel>>(particularProductsDto);
            return View("~/Views/Product/ParticularProductList.cshtml", products);
        }
    }
}