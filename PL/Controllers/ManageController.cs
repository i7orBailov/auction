﻿using BLL.DTO;
using PL.Models;
using PL.Filters;
using AutoMapper;
using System.Web;
using BLL.Interfaces;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace PL.Controllers
{
    [CustomAuthorize(checkPersonalInfo: false)]
    public class ManageController : Controller
    {
        readonly IManageService _manageService;
        readonly IMapper _mapper;
        IManageService ManageService
        {
            get
            {
                return _manageService ?? HttpContext.GetOwinContext().GetUserManager<IManageService>();
            }
        }

        IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ManageController(IManageService manageService, IMapper mapper)
        {
            _manageService = manageService;
            _mapper = mapper;
        }

        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPersonalInfoSuccess ? "Personal info has been set"
                : message == ManageMessageId.Error ? "An error has occurred."
                : string.Empty;

            var user = await ManageService.GetPersonalInfoByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                var model = _mapper.Map<PersonalInfoViewModel>(user);
                model.HasSetPersonalInfo = HasPersonalInfo(model);
                return View(model);
            }
            else
            return Content("Error with loading user (stop drop/creating database or join another method :/ ");
        }

        bool HasPersonalInfo(PersonalInfoViewModel model) =>
            !(model.LastName is null && model.FirstName is null && model.Address is null);

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await ManageService.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                AuthenticationManager.SignIn();
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        public ActionResult SetPersonalInfo()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPersonalInfo(PersonalInfoViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userDto = _mapper.Map<ClientDTO>(model);
                IdentityResult result = await ManageService.SetPersonalInfoAsync(userDto);
                if (result.Succeeded)
                {
                    AuthenticationManager.SignIn();
                    return RedirectToAction("Index", new { Message = ManageMessageId.SetPersonalInfoSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return View(model);
        }

        void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error);
            }
        }
    }

    public enum ManageMessageId
    {
        ChangePasswordSuccess,
        SetPersonalInfoSuccess,
        Error
    }
}