﻿using BLL.DTO;
using PL.Models;
using AutoMapper;
using PL.Filters;
using BLL.Interfaces;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace PL.Controllers
{
    [CustomExceptionHandler]
    public class ProductController : Controller
    {
        readonly IMapper _mapper;
        const string ERRORS = "errors";
        const string PRODUCT_INFO = "product";
        readonly IProductService _productService;

        public ProductController(IProductService serv, IMapper mapper)
        {
            _mapper = mapper;
            _productService = serv;
        }

        public async Task<ActionResult> ProductList(int? id)
        {
            IEnumerable<GeneralProductDTO> productDtos = await _productService.GetGeneralProductsAsync(id);
            var products = _mapper.Map<IEnumerable<GeneralProductDTO>, List<GeneralProductViewModel>>(productDtos);
            return View(products);
        }

        public async Task<ActionResult> ParticularProductList(int? id)
        {
            IEnumerable<ParticularProductDTO> particularProductsDto = await _productService.GetParticularProductListAsync(id);
            var products = _mapper.Map<IEnumerable<ParticularProductDTO>, IEnumerable< ParticularProductViewModel>>(particularProductsDto);
            return View(products);
        }

        
        public async Task<ActionResult> ParticularProduct(int? id)
        {
            ParticularProductDTO productDto = await _productService.GetParticularProductByIdAsync(id);
            var product = _mapper.Map<ParticularProductDTO, ParticularProductViewModel>(productDto);
            return View(product);
        }

        [CustomAuthorize]
        public ActionResult SellNewProduct(int? id)
        {
            var model = new SellNewProductViewModel() { GeneralProductId = id ?? 0 };
            return View(model);
        }

        [HttpPost]
        [CustomAuthorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SellNewProduct(SellNewProductViewModel product)
        {
            if (ModelState.IsValid)
            {
                if (product.Image != null)
                {
                    string folderPath = Server.MapPath($"~/Assets/UploadedFiles/{User.Identity.Name}");
                    string path = Path.Combine(Server.MapPath($"~/Assets/UploadedFiles/{User.Identity.Name}"),
                                  Path.GetFileName(product.Image.FileName));
                    Directory.CreateDirectory(folderPath);
                    product.Image.SaveAs(path);
                    product.ImageURL = $"~/Assets/UploadedFiles/{User.Identity.Name}/{product.Image.FileName}";
                }
                var productDto = _mapper.Map<ParticularProductDTO>(product);
                await _productService.SellNewProductAsync(productDto);
                return RedirectToAction(nameof(SentToProcessing));
            }
            return View(product);
        }

        [CustomAuthorize]
        public ActionResult SentToProcessing() => View();
    }
}