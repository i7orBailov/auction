﻿using System.ComponentModel.DataAnnotations;

namespace PL.Models
{
    public class LoginViewModel
    {
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Nick name")]
        public string NickName { get; set; }
    }

    public class RegisterViewModel
    {
        [Display(Name = "Nick name")]
        public string NickName { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        public string ConfirmPassword { get; set; }
    }
}