﻿using System.ComponentModel.DataAnnotations;

namespace PL.Models
{
    public class CustomerViewModel
    {
        public string Id { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }

        [Display(Name = "Contact mail")]
        public string Email { get; set; }

        [Display(Name = "Physical address")]
        public string Address { get; set; }

        [Display(Name = "Nick name")]
        public string NickName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }
    }
}