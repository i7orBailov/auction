﻿using System.ComponentModel.DataAnnotations;

namespace PL.Models
{
    public class ChangePasswordViewModel
    {   
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm new password")]
        public string ConfirmPassword { get; set; }
    }

    public class PersonalInfoViewModel
    {
        public string UserId { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Contact email")]
        public string Email { get; set; }

        [Display(Name = "Physical address")]
        public string Address { get; set; }

        public bool HasSetPersonalInfo { get; set; }
    }
}