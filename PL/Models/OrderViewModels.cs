﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PL.Models
{
    public class ShowOrderViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Display(Name = "Product")]
        public string ProductName { get; set; }

        public bool IsSentToCustomer { get; set; }

        [Display(Name = "Category")]
        public string ProductCategory { get; set; }

        [Display(Name = "Shipping address")]
        public string ShippingAddress { get; set; }

        [Display(Name = "Processed")]
        public DateTime ProcessingDate { get; set; }
    }
}