﻿using System;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PL.Models
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
    }

    public class GeneralProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string CategoryTitle { get; set; }
    }

    public class ParticularProductViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public decimal CurrentPrice { get; set; }
        public ICollection<string> Images { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int GeneralProductId { get; set; }
    }

    public class SellNewProductViewModel
    {
        public HttpPostedFileBase Image { get; set; }
        public string SellerId { get; set; }
        public int GeneralProductId { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Product name")]
        public string Name { get; set; }

        [Display(Name = "Price")]
        public decimal InitialPrice { get; set; }

        [Display(Name = "Expiration date")]
        public DateTime ExpirationDate { get; set; }

        public string ImageURL { get; set; }
    }

    public class FindProductViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Product name")]
        public string Name { get; set; }
    }
}