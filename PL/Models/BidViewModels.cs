﻿namespace PL.Models
{
    public class MakeBidViewModel
    {
        public string ClientId { get; set; }
        public int ProductId { get; set; }
        public decimal SuggestedPrice { get; set; }
        public decimal InitialPrice { get; set; }
        public string BidLastCustomerId { get; set; }
        public string ProductOwnerId { get; set; }
    }

    public class BidProductViewModel
    {
        public int Id { get; set; }
        public decimal SuggesterPrice { get; set; }
        public decimal InitialPrice { get; set; }
        public string CategoryTitle { get; set; }
        public string ProductDescription { get; set; }
        public string ProductName { get; set; }
    }
}