﻿using BLL.DTO;
using PL.Models;
using AutoMapper;
using System.Linq;

namespace PL.Util
{
    public class MapperProfilePL : Profile
    {
        public MapperProfilePL()
        {
            CreateMap<CategoryDTO, CategoryViewModel>();
            CreateMap<GeneralProductDTO, GeneralProductViewModel>()
                .ForMember(gp => gp.CategoryTitle, x => x.MapFrom(m => m.Category.Title))
                .ReverseMap();
            CreateMap<ParticularProductDTO, ParticularProductViewModel>()
                .ForMember(dest => dest.Images, opt => opt.MapFrom(src => src.Pictures.Select(p => p.URL)))
                .ReverseMap();
            CreateMap<BidDTO, MakeBidViewModel>()
                .ForMember(b => b.ProductId, x => x.MapFrom(m => m.ParticularProductId))
                .ReverseMap();
            CreateMap<ClientDTO, PersonalInfoViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ReverseMap();
            CreateMap<ParticularProductDTO, SellNewProductViewModel>()
                .ForMember(dest => dest.Image, opt => opt.Ignore())
                .ForMember(dest => dest.ImageURL, opt => opt.MapFrom(src => src.PictureURL))
                .ForMember(dest => dest.InitialPrice, opt => opt.MapFrom(src => src.CurrentPrice))
                .ReverseMap();
            CreateMap<ClientDTO, CustomerViewModel>()
                .ReverseMap();
            CreateMap<ShowOrderDTO, ShowOrderViewModel>()
                .ReverseMap();
        }
    }
}