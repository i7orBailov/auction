﻿using AutoMapper;
using BLL.Infrastructure;
using FluentValidation.Mvc;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using PL.Util;
using PL.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PL
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectModule registrationsBLL = new ServiceModuleBLL();
            var kernelBLL = new StandardKernel(registrationsBLL);
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MapperProfilePL());
                cfg.AddProfile(new MapperProfileBLL());
            });
            kernelBLL.Unbind<ModelValidatorProvider>();
            kernelBLL.Bind<IMapper>().ToConstant(mapperConfig.CreateMapper());
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernelBLL));

            ValidatorConfiguration();
        }

        void ValidatorConfiguration()
        {
            FluentValidationModelValidatorProvider.Configure(provider =>
            {
                provider.ValidatorFactory = new FluentValidatorFactory();
            });

            ModelValidatorProviders.Providers.Remove(
            ModelValidatorProviders.Providers.OfType<DataAnnotationsModelValidatorProvider>().First());
        }
    }
}
