﻿let eye_closed = "eye-closed",
    eye_opened = "eye-opened",
    state = false;

addEvent(eye_closed);
addEvent(eye_opened);

function addEvent(attribute) {
    document.getElementById(attribute).addEventListener("click", () => toggle());
}

function toggle() {
    if (state) {
        setInputType("password");
        setEye(eye_opened, eye_closed);
        state = false;
    }
    else {
        setInputType("text");
        setEye(eye_closed, eye_opened)
        state = true;
    }
};

function setInputType(type) {
    document.getElementById("password").setAttribute("type", type);
}

function setEye(active_eye, non_active_eye) {
    document.getElementById(active_eye).classList.add("active");
    document.getElementById(non_active_eye).classList.remove("active");
}